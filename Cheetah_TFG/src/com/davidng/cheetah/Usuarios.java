package com.davidng.cheetah;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Usuarios {
    private int id;
    private String nombre;
    private String apellidos;
    private String email;
    private Date fechaNacimiento;
    private String nick;
    private String passwd;
    private String permiso;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    /**
     * Metodo con el que devolvemos el nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo con el que dar valor al nombre
     *
     * @param nombre Variable con la que introduciremos el nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    /**
     * Metodo con el que devolvemos los apellidos
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * Metodo con el que dar valor a los apellidos
     *
     * @param apellidos Variable con la que introduciremos los apellidos
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "email")
    /**
     * Metodo con el que devolvemos el email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Metodo con el que dar valor al email
     *
     * @param email Variable con la que introduciremos el email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "fecha_nacimiento")
    /**
     * Metodo con el que devolvemos la fecha de nacimiento
     */
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * Metodo con el que dar valor a la fecha de nacimiento
     *
     * @param fechaNacimiento Variable con la que introduciremos la fecha de nacimiento
     */
    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Basic
    @Column(name = "nick")
    /**
     * Metodo con el que devolvemos el nick
     */
    public String getNick() {
        return nick;
    }

    /**
     * Metodo con el que dar valor al nick
     *
     * @param nick Variable con la que introduciremos el nick
     */
    public void setNick(String nick) {
        this.nick = nick;
    }

    @Basic
    @Column(name = "passwd")
    /**
     * Metodo con el que devolvemos la contraseña
     */
    public String getPasswd() {
        return passwd;
    }

    /**
     * Metodo con el que dar valor a la contraseña
     * @param passwd Variable con la que introduciremos la contraseña
     */
    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    @Basic
    @Column(name = "permiso")
    /**
     * Metodo con el que devolvemos el permiso
     */
    public String getPermiso() {
        return permiso;
    }

    /**
     * Metodo con el que dar valor al permiso
     * @param permiso Variable con la que introduciremos el permiso
     */
    public void setPermiso(String permiso) {
        this.permiso = permiso;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuarios usuarios = (Usuarios) o;
        return id == usuarios.id &&
                Objects.equals(nombre, usuarios.nombre) &&
                Objects.equals(apellidos, usuarios.apellidos) &&
                Objects.equals(email, usuarios.email) &&
                Objects.equals(fechaNacimiento, usuarios.fechaNacimiento) &&
                Objects.equals(nick, usuarios.nick) &&
                Objects.equals(passwd, usuarios.passwd) &&
                Objects.equals(permiso, usuarios.permiso);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, email, fechaNacimiento, nick, passwd, permiso);
    }

    @Override
    public String toString() {
        return nick +
                ", " + permiso;
    }
}
