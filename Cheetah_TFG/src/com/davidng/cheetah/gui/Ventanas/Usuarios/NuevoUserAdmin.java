package com.davidng.cheetah.gui.Ventanas.Usuarios;

import com.davidng.cheetah.Enum.Permisos;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * @author dnadeau
 * Clase donde tendremos la ventana para crear un nuevo usuario
 */
public class NuevoUserAdmin extends JFrame {


    private JPanel panelNuevUserAdm;
    public JButton atrasUserButton;
    public JButton nuevoUserButton;

    public JTextField txtNomUserNuev;
    public JTextField txtApeUserNuev;
    public JTextField txtEmaiUserNuev;
    public JTextField txtNickUserNuev;

    public JPasswordField txtPasswdUserNuev;

    public JComboBox cbPermUserNuev;

    public DatePicker fechaNacNuevUser;

    /**
     * Constructor de la clase donde se inicializaran los datos
     */
    public NuevoUserAdmin() {
        this.setTitle("CHEETAH");
        initDialog();
        setComboBoxDatosUser();
        controlCantCaracteres();
    }

    /**
     * Metodo donde inicializaremos la ventana
     */
    private void initDialog() {
        this.setContentPane(panelNuevUserAdm);
        this.panelNuevUserAdm.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth() + 500, this.getHeight() + 100));
        this.setVisible(false);
        this.setIconImage(getIconImage());
        this.setLocationRelativeTo(null);
    }

    /**
     * Metodo con el que introducimos los datos de la clase enumerada Permisos
     * a la combobox
     */
    private void setComboBoxDatosUser() {
        for (Permisos permisos : Permisos.values()) {
            cbPermUserNuev.addItem(permisos.getPermiso());
        }
        cbPermUserNuev.setSelectedItem(-1);
    }

    /**
     * Metodo con el que se controlará la cantidad de caracteres introducidos en el campo
     */
    private void controlCantCaracteres() {
        txtNomUserNuev.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtNomUserNuev.getText().length() > 49) {
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtApeUserNuev.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtApeUserNuev.getText().length() > 99) {
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtEmaiUserNuev.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtEmaiUserNuev.getText().length() > 149) {
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtNickUserNuev.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtNickUserNuev.getText().length() > 49) {
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtPasswdUserNuev.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtPasswdUserNuev.getText().length() > 49) {
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
    }

    /**
     * Metodo que nos mostrara un icono en la parte superior izquierda de la ventana
     * @return retValue
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("cheetah.png"));


        return retValue;
    }
}
