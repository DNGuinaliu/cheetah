package com.davidng.cheetah.gui.Ventanas.Usuarios;

import com.davidng.cheetah.Enum.Permisos;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

/**
 * @author dnadeau
 * Clase donde tendremos la ventana para visualizar un usuario
 */
public class VisualizarUsuario extends JFrame {
    private JPanel panel1;
    public JTextField txtNomUser;
    public JTextField txtApell;
    public JTextField txtEmail;
    public JTextField txtNick;
    public JPasswordField txtPasswd;
    public JComboBox cbPermiso;
    public DatePicker fechaNac;
    public JButton atrasButton;

    /**
     * Constructor de la clase donde se inicializaran los datos
     */
    public VisualizarUsuario() {
        this.setTitle("CHEETAH");
        initDialog();
        setComboBoxDatosUser();
        getIconImage();
    }

    /**
     * Metodo donde inicializaremos la ventana
     */
    private void initDialog() {
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth() + 500, this.getHeight() + 100));
        this.setVisible(false);
        this.setIconImage(getIconImage());
        this.setLocationRelativeTo(null);
    }

    /**
     * Metodo con el que introducimos los datos de la clase enumerada Permisos
     * a la combobox
     */
    private void setComboBoxDatosUser() {
        for (Permisos permisos : Permisos.values()) {
            cbPermiso.addItem(permisos.getPermiso());
        }
        cbPermiso.setSelectedItem(-1);
    }

    /**
     * Metodo que nos mostrara un icono en la parte superior izquierda de la ventana
     *
     * @return retValue
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("cheetah.png"));


        return retValue;
    }
}
