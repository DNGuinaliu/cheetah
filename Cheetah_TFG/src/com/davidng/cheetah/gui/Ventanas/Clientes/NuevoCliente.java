package com.davidng.cheetah.gui.Ventanas.Clientes;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * @author dnadeau
 * Clase donde tendremos la ventana para crear un nuevo cliente
 */
public class NuevoCliente extends JFrame {
    private JPanel panelNuvClien;

    public JButton nuevoClienteButton;
    public JButton atrasClientButton;

    public JTextField txtNomClien;
    public JTextField txtApeClien;
    public JTextField txtDniClient;
    public JTextField txtTelefClien;
    public JTextField txtEmailClien;
    public JTextField txtDireccClien;
    public JTextField txtCPClien;
    public DatePicker fechaNacClien;

    /**
     * Constructor de la clase donde se inicializaran los datos
     */
    public NuevoCliente() {
        this.setTitle("CHEETAH");
        initDialog();
        controlCantCaracteres();
    }

    /**
     * Metodo donde inicializaremos la ventana
     */
    private void initDialog() {
        this.setContentPane(panelNuvClien);
        this.panelNuvClien.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth() + 500, this.getHeight() + 100));
        this.setVisible(false);
        this.setIconImage(getIconImage());
        this.setLocationRelativeTo(null);
    }

    /**
     * Metodo con el que se controlará la cantidad de caracteres introducidos en el campo
     */
    private void controlCantCaracteres() {
        txtDniClient.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtDniClient.getText().length() > 8) {
                    Toolkit.getDefaultToolkit().beep();
                    k.consume();
                }
            }
        });
        txtNomClien.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtNomClien.getText().length() > 49) {
                    Toolkit.getDefaultToolkit().beep();
                    k.consume();
                }
            }
        });
        txtApeClien.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtApeClien.getText().length() > 99) {
                    Toolkit.getDefaultToolkit().beep();
                    k.consume();
                }
            }
        });
        txtEmailClien.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtEmailClien.getText().length() > 99) {
                    Toolkit.getDefaultToolkit().beep();
                    k.consume();
                }
            }
        });
        txtDireccClien.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtDireccClien.getText().length() > 99) {
                    Toolkit.getDefaultToolkit().beep();
                    k.consume();
                }
            }
        });
    }

    /**
     * Metodo que nos mostrara un icono en la parte superior izquierda de la ventana
     *
     * @return retValue
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("cheetah.png"));
        return retValue;
    }
}
