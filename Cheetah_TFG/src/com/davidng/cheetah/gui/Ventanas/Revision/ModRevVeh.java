package com.davidng.cheetah.gui.Ventanas.Revision;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

/**
 * @author dnadeau
 * Clase en la cual podremos modificar un cliente mediante una ventana
 */
public class ModRevVeh extends JFrame {
    private JPanel panelModRev;
    public JButton atrasRevButton;
    public JButton guardarRevButton;
    public DatePicker fechaRadiador;
    public DatePicker fechaAceite;
    public DatePicker fechaFiltro;
    public DatePicker fechaNeumatico;
    public DatePicker fechaAmortiguador;
    public DatePicker fechaFreno;
    public DatePicker fechaLuces;
    public JComboBox cbVehiculoRevisionMod;

    /**
     * Constructor de la clase donde se inicializaran los datos
     */
    public ModRevVeh() {
        this.setTitle("CHEETAH");
        initDialog();
        getIconImage();
    }

    /**
     * Metodo donde inicializaremos la ventana
     */
    private void initDialog() {
        this.setContentPane(panelModRev);
        this.panelModRev.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth() + 500, this.getHeight() + 100));
        this.setVisible(false);
        this.setIconImage(getIconImage());
        this.setLocationRelativeTo(null);
    }

    /**
     * Metodo que nos mostrara un icono en la parte superior izquierda de la ventana
     *
     * @return retValue
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("cheetah.png"));


        return retValue;
    }
}
