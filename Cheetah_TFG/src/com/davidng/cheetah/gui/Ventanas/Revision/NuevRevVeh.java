package com.davidng.cheetah.gui.Ventanas.Revision;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

/**
 * @author dnadeau
 * Clase donde tendremos la ventana para crear una nueva revisión
 */
public class NuevRevVeh extends JFrame {
    private JPanel panelNuvRev;
    public JButton atrasRevNvButton;
    public JButton nuevoRevButton;
    public DatePicker fechaRadiador;
    public DatePicker fechaAceite;
    public DatePicker fechaFrenos;
    public DatePicker fechaLuces;
    public DatePicker fechaFiltro;
    public DatePicker fechaNeumaticos;
    public DatePicker fechaAmortiguador;
    public JComboBox cbVehiculosRevision;

    /**
     * Constructor de la clase donde se inicializaran los datos
     */
    public NuevRevVeh() {
        this.setTitle("CHEETAH");
        initDialog();
        getIconImage();
    }

    /**
     * Metodo donde inicializaremos la ventana
     */
    private void initDialog() {
        this.setContentPane(panelNuvRev);
        this.panelNuvRev.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth() + 500, this.getHeight() + 100));
        this.setVisible(false);
        this.setIconImage(getIconImage());
        this.setLocationRelativeTo(null);
    }

    /**
     * Metodo que nos mostrara un icono en la parte superior izquierda de la ventana
     *
     * @return retValue
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("cheetah.png"));


        return retValue;
    }

}
