package com.davidng.cheetah.gui.Ventanas.Revision;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

/**
 * @author dnadeau
 * Clase donde tendremos una ventana para visualizar los datos de la revisión
 */
public class VisualizarRevision extends JFrame {


    private JPanel panel1;
    public JButton atrasButton;
    public DatePicker fechaRadiador;
    public DatePicker fechaFiltro;
    public DatePicker fechaNeumatico;
    public DatePicker fechaAmortiguador;
    public DatePicker fechaAceite;
    public DatePicker fechaFrenos;
    public DatePicker fechaLuces;

    /**
     * Constructor de la clase donde se inicializaran los datos
     */
    public VisualizarRevision(){
        this.setTitle("CHEETAH");
        initDialog();
        getIconImage();
    }

    /**
     * Metodo donde inicializaremos la ventana
     */
    private void initDialog() {
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth() + 500, this.getHeight() + 100));
        this.setVisible(false);
        this.setIconImage(getIconImage());
        this.setLocationRelativeTo(null);
    }

    /**
     * Metodo que nos mostrara un icono en la parte superior izquierda de la ventana
     * @return retValue
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("cheetah.png"));


        return retValue;
    }
}
