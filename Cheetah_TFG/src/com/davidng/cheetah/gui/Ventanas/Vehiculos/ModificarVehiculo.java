package com.davidng.cheetah.gui.Ventanas.Vehiculos;

import com.davidng.cheetah.Enum.Marcas;
import com.davidng.cheetah.Enum.Motor;
import com.davidng.cheetah.Enum.TipoVeh;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * @author dnadeau
 * Clase en la cual podremos modificar un vehiculo mediante una ventana
 */
public class ModificarVehiculo extends JFrame {
    private JPanel panelModVeh;
    public JButton atrasModVehButton;
    public JButton guardarVehModButton;

    public JTextField txtPotencia;
    public JTextField txtModelo;
    public JTextField txtDomVeh;
    public JTextField txtColor;

    public DatePicker fechaFabModVeh;

    public JComboBox cbModVeh;
    public JComboBox cbMotor;
    public JComboBox cbTipoVeh;

    /**
     * Constructor de la clase donde se inicializaran los datos
     */
    public ModificarVehiculo() {
        this.setTitle("CHEETAH");
        initDialog();
        setComboBoxDatosVehiculos();
        controlCantCaracteres();
        getIconImage();
    }

    /**
     * Metodo donde inicializaremos la ventana
     */
    private void initDialog() {
        this.setContentPane(panelModVeh);
        this.panelModVeh.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth() + 500, this.getHeight() + 100));
        this.setVisible(false);
        this.setIconImage(getIconImage());
        this.setLocationRelativeTo(null);
    }

    /**
     * Metodo que nos mostrara un icono en la parte superior izquierda de la ventana
     *
     * @return retValue
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("cheetah.png"));


        return retValue;
    }

    /**
     * Metodo con el que introduciremos los datos de las clases enumeradas en las combobox
     */
    private void setComboBoxDatosVehiculos() {
        for (Marcas marcas : Marcas.values()) {
            cbModVeh.addItem(marcas.getMarca());
        }
        cbModVeh.setSelectedItem(-1);

        for (Motor motor : Motor.values()) {
            cbMotor.addItem(motor.getMotor());
        }
        cbMotor.setSelectedItem(-1);

        for (TipoVeh tipoVeh : TipoVeh.values()){
            cbTipoVeh.addItem(tipoVeh.getTipveh());
        }
        cbTipoVeh.setSelectedItem(0);
    }

    /**
     * Metodo con el que se controlará la cantidad de caracteres introducidos en el campo
     */
    private void controlCantCaracteres(){
        txtModelo.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtModelo.getText().length()>99){
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtDomVeh.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtDomVeh.getText().length()>99){
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtColor.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtColor.getText().length()>49){
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
    }

}
