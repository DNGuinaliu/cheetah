package com.davidng.cheetah.gui.Ventanas.Carga;


import javax.swing.*;
import java.awt.*;

/**
 * @author dnadeau
 * Clase la cual saldra la primera al ejecutar el programa en forma de carga
 */
public class VentanaCarga extends JFrame {
    private JPanel panel1;
    public JProgressBar barra;
    public JLabel carga;
    private Thread t;

    /**
     * Constructor de la clase en la que inicializaremos los datos
     */
    public VentanaCarga() {
        this.setTitle("CHEETAH");
        initDialog();
        getIconImage();
        try {
            carga();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo donde inicializaremos la ventana
     */
    private void initDialog() {
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth() + 100, this.getHeight()));
        this.setVisible(true);
        this.setIconImage(getIconImage());
        this.setLocationRelativeTo(null);

    }

    /**
     * Metodo que nos mostrara un icono en la parte superior izquierda de la ventana
     *
     * @return retValue
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("cheetah.png"));

        return retValue;
    }

    /**
     * Metodo con el que se hará la función de carga y cada x tiempo nos mostrara un mensaje diferente
     *
     * @throws InterruptedException
     */
    private void carga() throws InterruptedException {
        for (int i = 0; i <= 100; i++) {
            t.sleep(100);
            if (i == 1) {
                carga.setText("CONNECTING...");
            }
            if (i == 30) {
                carga.setText("CONNECTING TO DATABASE...");
            }
            if (i == 50) {
                carga.setText("CONNECTION SUCCESSFUL!");
            }
            if (i == 80) {
                carga.setText("LAUNCHING APPLICATION...");
            }
            if (i == 100) {
                setVisible(false);
                dispose();
            }
            barra.setValue(i);
            barra.setStringPainted(true);
        }
    }
}
