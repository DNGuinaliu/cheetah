package com.davidng.cheetah.gui.Ventanas;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * @author dnadeau
 * Clase donde tendremos la ventana principal de la aplicación
 */
public class Administrador extends JFrame {
    private JTabbedPane tabbedPane1;
    private JPanel panel;

    //Usuarios
    public JList listaUsuarios;
    public JButton eliminarUsuariosButton;
    public JButton modicarButton;
    public JButton crearUsuarioMod;
    public JButton visualizarDatosUserButton;

    //Clientes
    public JButton crearClienteButton;
    public JButton eliminarClienteButton;
    public JButton modificarClienteButton;
    public JList listaClientes;
    public JButton informesClienteButton;
    public JButton visualizarDatosCliButton;
    public JTextField txtBuscarCliente;
    public JButton buscarClienteButton;

    //Vehiculos
    public JButton nuevoVehiculoButton;
    public JButton modificarVehiculoButton;
    public JButton eliminarVehiculoButton;
    public JList listaVehiculosAdmin;
    public JButton informesVehiculoButton;
    public JButton visualizarDatosVehButton;
    public JButton buscarVehiculoButton;
    public JTextField txtVehBuscar;

    //Revision
    public JButton nuevaRevisionButton;
    public JButton modificarRevisionButton;
    public JButton eliminarRevisionButton;
    public JList listaRevision;
    public JButton informesRevisionButton;
    public JButton visualizarDatosRevButton;

    //Venta
    public JButton crearVentaButton;
    public JButton modificarVentaButton;
    public JButton eliminarVentaButton;
    public JList listaVenta;
    public JList listaVentaVehiculo;
    public JList listaVehiculoVenta;
    public JButton añadirVehVenButton;
    public JButton listarButton;
    public JButton informesVentasButton;
    public JButton visualizarDatosVenButton;
    public JButton facturaVentaButton;
    public JButton buscarVentaButton;
    public JTextField txtBuscarVenta;

    //Listas
    public DefaultListModel dlmUsuarios;
    public DefaultListModel dlmClientes;
    public DefaultListModel dlmVehiculos;
    public DefaultListModel dlmRevision;
    public DefaultListModel dlmVenta;
    public DefaultListModel dlmVentaVehiculo;
    public DefaultListModel dlmVehiculoVenta;

    /**
     * Constructor de la clase donde se inicializaran los datos
     */
    public Administrador() {
        this.setTitle("CHEETAH");
        initDialog();
        dlm();
    }

    /**
     * Metodo donde inicializaremos la ventana
     */
    private void initDialog() {
        this.setContentPane(panel);
        this.panel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth(), this.getHeight() + 100));
        this.setVisible(false);
        this.setIconImage(getIconImage());
        this.setLocationRelativeTo(null);
    }

    /**
     * Metodo donde inicializaremos los DefaultListModel
     */
    private void dlm() {
        dlmUsuarios = new DefaultListModel();
        listaUsuarios.setModel(dlmUsuarios);

        dlmClientes = new DefaultListModel();
        listaClientes.setModel(dlmClientes);

        dlmVehiculos = new DefaultListModel();
        listaVehiculosAdmin.setModel(dlmVehiculos);

        dlmRevision = new DefaultListModel();
        listaRevision.setModel(dlmRevision);

        dlmVenta = new DefaultListModel();
        listaVenta.setModel(dlmVenta);

        dlmVentaVehiculo = new DefaultListModel();
        listaVentaVehiculo.setModel(dlmVentaVehiculo);

        dlmVehiculoVenta = new DefaultListModel();
        listaVehiculoVenta.setModel(dlmVehiculoVenta);
    }

    /**
     * Metodo que nos mostrara un icono en la parte superior izquierda de la ventana
     *
     * @return retValue
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("cheetah.png"));


        return retValue;
    }

}
