package com.davidng.cheetah.gui.Ventanas.Ventas;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 *  @author dnadeau
 *  Clase en la cual podremos modificar una venta mediante una ventana
 */
public class ModificarVenta extends JFrame{
    private JPanel panelModVen;

    public JButton atrasModVenButton;
    public JButton guardarModVenButton;

    public JTextField txtConsulta;
    public JTextField txtIVA;
    public JTextField txtPrecio;
    public JTextField txtObservaciones;
    public JTextField txtCodVenta;
    public JTextField txtDescripcion;

    public JComboBox cbCliente;
    public DatePicker fechaVenta;

    /**
     * Constructor de la clase donde se inicializaran los datos
     */
    public ModificarVenta(){
        this.setTitle("CHEETAH");
        initDialog();
        controlCantCaracteres();
    }

    /**
     * Metodo donde inicializaremos la ventana
     */
    private void initDialog() {
        this.setContentPane(panelModVen);
        this.panelModVen.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth() + 500, this.getHeight() + 100));
        this.setVisible(false);
        this.setIconImage(getIconImage());
        this.setLocationRelativeTo(null);
    }

    /**
     * Metodo que nos mostrara un icono en la parte superior izquierda de la ventana
     * @return retValue
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("cheetah.png"));


        return retValue;
    }

    /**
     * Metodo con el que se controlará la cantidad de caracteres introducidos en el campo
     */
    private void controlCantCaracteres(){
        txtConsulta.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtConsulta.getText().length()>34){
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtObservaciones.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtObservaciones.getText().length()>249){
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtDescripcion.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtDescripcion.getText().length()>149){
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
    }
}
