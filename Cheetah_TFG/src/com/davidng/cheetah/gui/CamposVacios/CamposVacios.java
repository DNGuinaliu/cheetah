package com.davidng.cheetah.gui.CamposVacios;

import com.davidng.cheetah.gui.Usuarios.NuevoUsuario;
import com.davidng.cheetah.gui.Usuarios.VistaUsuarios;
import com.davidng.cheetah.gui.Ventanas.Clientes.ModificarCliente;
import com.davidng.cheetah.gui.Ventanas.Clientes.NuevoCliente;
import com.davidng.cheetah.gui.Ventanas.Revision.ModRevVeh;
import com.davidng.cheetah.gui.Ventanas.Revision.NuevRevVeh;
import com.davidng.cheetah.gui.Ventanas.Usuarios.ModificarUsuario;
import com.davidng.cheetah.gui.Ventanas.Usuarios.NuevoUserAdmin;
import com.davidng.cheetah.gui.Ventanas.Vehiculos.ModificarVehiculo;
import com.davidng.cheetah.gui.Ventanas.Vehiculos.NuevoVehiculo;
import com.davidng.cheetah.gui.Ventanas.Ventas.ModificarVenta;
import com.davidng.cheetah.gui.Ventanas.Ventas.NuevaVenta;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author dnadeau
 * Clase donde tendremos los diferentes metodos que nos comprobaran si existe algun campo vacio
 */
public class CamposVacios {

    //USUARIO

    /**
     * Metodo que me comprueba si existe un campo vacio en el login
     *
     * @param usuario Objeto de la clase VistaUsuario
     * @return nos devuelve si existe un campo vacio en el login
     */
    public boolean camposVaciosLogin(VistaUsuarios usuario) {
        return usuario.txtNombreUsuario.getText().isEmpty() ||
                usuario.txtContraseña.getText().isEmpty();
    }

    /**
     * Metodo que me comprueba si existe un campo vacio en la ventana para crear
     * un usuario nuevo si no existe ninguno
     *
     * @param nuevoUsuario Objeto de la clase NuevoUsuario
     * @return nos devuelve si existe un campo vacio en la ventana para crear un
     * usuario nuevo si no existe ninguno
     */
    public boolean camposVacios(NuevoUsuario nuevoUsuario) {
        return nuevoUsuario.txtNombreUsuario.getText().isEmpty() ||
                nuevoUsuario.txtNuevaContraseña.getText().isEmpty() ||
                nuevoUsuario.txtEmailUsuario.getText().isEmpty() ||
                nuevoUsuario.fechaNacUser.getText().isEmpty() ||
                nuevoUsuario.txtApellidoUsuario.getText().isEmpty() ||
                nuevoUsuario.txtNickUsuario.getText().isEmpty() ||
                nuevoUsuario.cbPermisoUser.getSelectedIndex() == -1;

    }

    /**
     * Metodo que me comprueba si existe un campo vacio en la ventana de crear un usuario desde administrador
     *
     * @param nUserAdm Objeto de la clase NuevoUserAdmin
     * @return nos devuelve si existe un campo vacio a la hora de crear un usuario nuevo
     * desde la ventana de administrador
     */
    public boolean camposVaciosNuevUserAdmin(NuevoUserAdmin nUserAdm) {
        return nUserAdm.txtNomUserNuev.getText().isEmpty() ||
                nUserAdm.txtApeUserNuev.getText().isEmpty() ||
                nUserAdm.txtEmaiUserNuev.getText().isEmpty() ||
                nUserAdm.fechaNacNuevUser.getText().isEmpty() ||
                nUserAdm.txtNickUserNuev.getText().isEmpty() ||
                nUserAdm.txtPasswdUserNuev.getText().isEmpty() ||
                nUserAdm.cbPermUserNuev.getSelectedIndex() == -1;
    }


    /**
     * Metodo que me comprueba si existe un campo vacio en la ventana de modificar un usuario desde administrador
     *
     * @param modUser Objeto de la clase ModificarUsuario
     * @return nos devuelve si existe un campo vacio a la hora de modificar un usuario
     * desde administrador
     */
    public boolean camposVaciosModificarUser(ModificarUsuario modUser) {
        return modUser.txtNombUserMod.getText().isEmpty() ||
                modUser.txtApellMod.getText().isEmpty() ||
                modUser.txtEmailMod.getText().isEmpty() ||
                modUser.fechaNacModUser.getText().isEmpty() ||
                modUser.txtNickMod.getText().isEmpty() ||
                modUser.txtApellMod.getText().isEmpty() ||
                modUser.cbPermMod.getSelectedIndex() == -1;
    }

    //CLIENTE

    /**
     * Metodo que me comprueba si existe un campo vacio en la ventana de crear un cliente
     *
     * @param nClient Objeto de la clase NuevoCliente
     * @return nos devuelve si existe un campo vacio a la hora de crear un cliente
     */
    public boolean camposVaciosNuevClient(NuevoCliente nClient) {
        return nClient.txtNomClien.getText().isEmpty() ||
                nClient.txtApeClien.getText().isEmpty() ||
                nClient.txtDniClient.getText().isEmpty() ||
                nClient.fechaNacClien.getText().isEmpty() ||
                nClient.txtEmailClien.getText().isEmpty() ||
                nClient.txtTelefClien.getText().isEmpty() ||
                nClient.txtDireccClien.getText().isEmpty() ||
                nClient.txtCPClien.getText().isEmpty();
    }

    /**
     * Metodo que me comprueba si existe un campo vacio en la ventana de modificar un cliente
     *
     * @param mClient Objeto de la clase ModificarCliente
     * @return nos devuelve si existe un campo vacio a la hora de modificar un cliente
     */
    public boolean camposVaciosModClient(ModificarCliente mClient) {
        return mClient.txtNombreClienMod.getText().isEmpty() ||
                mClient.txtApeClienMod.getText().isEmpty() ||
                mClient.txtDniClienMod.getText().isEmpty() ||
                mClient.FechaNacClienMod.getText().isEmpty() ||
                mClient.txtEmailClienMod.getText().isEmpty() ||
                mClient.txtTelClienMod.getText().isEmpty() ||
                mClient.txtDireCienMod.getText().isEmpty() ||
                mClient.txtCPClienMod.getText().isEmpty();
    }

    //REVISION

    /**
     * Metodo que me comprueba si existe un campo vacio en la ventana de crear una revisión
     *
     * @param nRevVeh Objeto de la clase NuevRevVeh
     * @return nos devuelve si existe un campo vacio a la hora de crear una revisión
     */
    public boolean camposVaciosNuvRev(NuevRevVeh nRevVeh) {
        return nRevVeh.fechaAceite.getText().isEmpty() ||
                nRevVeh.fechaAmortiguador.getText().isEmpty() ||
                nRevVeh.fechaFiltro.getText().isEmpty() ||
                nRevVeh.fechaFrenos.getText().isEmpty() ||
                nRevVeh.fechaLuces.getText().isEmpty() ||
                nRevVeh.fechaNeumaticos.getText().isEmpty() ||
                nRevVeh.fechaRadiador.getText().isEmpty() ||
                nRevVeh.cbVehiculosRevision.getSelectedIndex() == -1;
    }

    /**
     * Metodo que me comprueba si existe un campo vacio en la ventana de modificar una revisión
     *
     * @param modRevVeh Objeto de la clase ModRevVeh
     * @return nos devuelve si existe un campo vacio a la hora de modificar una revisión
     */
    public boolean camposVaciosModRev(ModRevVeh modRevVeh) {
        return modRevVeh.fechaAceite.getText().isEmpty() ||
                modRevVeh.fechaAmortiguador.getText().isEmpty() ||
                modRevVeh.fechaFiltro.getText().isEmpty() ||
                modRevVeh.fechaFreno.getText().isEmpty() ||
                modRevVeh.fechaLuces.getText().isEmpty() ||
                modRevVeh.fechaNeumatico.getText().isEmpty() ||
                modRevVeh.fechaRadiador.getText().isEmpty() ||
                modRevVeh.cbVehiculoRevisionMod.getSelectedIndex() == -1;
    }

    //VEHICULO

    /**
     * Metodo que me comprueba si existe un campo vacio en la ventana de crear un vehiculo
     *
     * @param nVehiculo Objeto de la clase NuevoVehiculo
     * @return nos devuelve si existe un campo vacio a la hora de crear un vehiculo
     */
    public boolean camposVaciosVeh(NuevoVehiculo nVehiculo) {
        return nVehiculo.cbMarcaVeh.getSelectedIndex() == -1 ||
                nVehiculo.txtModeloVeh.getText().isEmpty() ||
                nVehiculo.cbTipoVeh.getSelectedIndex() == -1 ||
                nVehiculo.txtDomicilio.getText().isEmpty() ||
                nVehiculo.cbMotor.getSelectedIndex() == -1 ||
                nVehiculo.txtPotencia.getText().isEmpty() ||
                nVehiculo.txtColor.getText().isEmpty() ||
                nVehiculo.fechaFabriVeh.getText().isEmpty();

    }

    /**
     * Metodo que me comprueba si existe un campo vacio en la ventana de modificar un vehiculo
     *
     * @param mVehiculo Objeto de la clase ModificarVehiculo
     * @return nos devuelve si existe un campo vacio a la hora de modificar un vehiculo
     */
    public boolean camposVaciosModVeh(ModificarVehiculo mVehiculo) {
        return mVehiculo.cbModVeh.getSelectedIndex() == -1 ||
                mVehiculo.txtModelo.getText().isEmpty() ||
                mVehiculo.cbTipoVeh.getSelectedIndex() == -1 ||
                mVehiculo.txtDomVeh.getText().isEmpty() ||
                mVehiculo.cbMotor.getSelectedIndex() == -1 ||
                mVehiculo.txtPotencia.getText().isEmpty() ||
                mVehiculo.txtColor.getText().isEmpty() ||
                mVehiculo.fechaFabModVeh.getText().isEmpty();
    }

    //VENTA

    /**
     * Metodo que me comprueba si existe un campo vacio en la ventana de crear una venta
     *
     * @param nVenta Objeto de la clase NuevaVenta
     * @return nos devuelve si existe un campo vacio a la hora de crear una venta
     */
    public boolean camposVaciosVent(NuevaVenta nVenta) {
        return nVenta.txtConsulta.getText().isEmpty() ||
                nVenta.txtCodVenta.getText().isEmpty() ||
                nVenta.txtIVA.getText().isEmpty() ||
                nVenta.fechaVenta.getText().isEmpty() ||
                nVenta.txtPrecio.getText().isEmpty() ||
                nVenta.cbCliente.getSelectedIndex() == -1 ||
                nVenta.txtObservaciones.getText().isEmpty() ||
                nVenta.txtDescripcion.getText().isEmpty();
    }

    /**
     * Metodo que me comprueba si existe un campo vacio en la ventana de modificar una venta
     *
     * @param mVenta Objeto de la clase ModificarVenta
     * @return nos devuelve si existe un campo vacio a la hora de modificar una venta
     */
    public boolean camposVaciosModVent(ModificarVenta mVenta) {
        return mVenta.txtConsulta.getText().isEmpty() ||
                mVenta.txtCodVenta.getText().isEmpty() ||
                mVenta.txtIVA.getText().isEmpty() ||
                mVenta.fechaVenta.getText().isEmpty() ||
                mVenta.txtPrecio.getText().isEmpty() ||
                mVenta.cbCliente.getSelectedIndex() == -1 ||
                mVenta.txtObservaciones.getText().isEmpty() ||
                mVenta.txtDescripcion.getText().isEmpty();
    }

    /**
     * Metodo con el que se comprobará si el dni pasado como parámetro esta correctamente escrito
     *
     * @param dni Variable con la que comprobaremos si el dni es correcto
     * @return nos devuelve si el dni esta correctamente escrito o no
     */
    public boolean comprobanteDni(String dni) {
        Pattern pattern = null;
        Matcher matcher = null;
        pattern = Pattern.compile("[0-9]{8,9}[A-Z]");
        matcher = pattern.matcher(dni);
        if (matcher.find()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Metodo con el que se comprobará si el email pasado como parámetro esta correctamente escrito
     *
     * @param email Variable con la que comprobaremos si el email es correcto
     * @return nos devuelve si el email esta correctamente escrito o no
     */
    public boolean comprobanteEmail(String email) {
        Pattern pat = Pattern.compile("([a-z0-9]+(\\.?[a-z0-9])*)+@(([a-z]+)\\.([a-z]+))+");


        Matcher matcher = pat.matcher(email);
        if (matcher.find()) {
            return true;
        } else {
            return false;
        }
    }

}
