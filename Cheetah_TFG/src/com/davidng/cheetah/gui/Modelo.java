package com.davidng.cheetah.gui;

import com.davidng.cheetah.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;

/**
 * @author dnadeau
 * Clase en la que crearemos los metodos para dar de alta, eliminar y modificar, además
 * de la llamada a funciones de la base de datos
 */
public class Modelo {

    public static Connection conexion; //Variable mediante la cual nos conectaremos a la base de datos
    SessionFactory sessionFactory; //Variable con la que hacemos llamada a la clase Session con la que podremos realizar diferentes funciones con las entidades

    /**
     * Metodo con el que podremos conectar a la base de datos y a las entidades generadas
     */
    public void conectar() {

        Configuration configuracion = new Configuration();
        configuracion.configure("hibernate.cfg.xml");

        configuracion.addAnnotatedClass(Revision.class);
        configuracion.addAnnotatedClass(Vehiculo.class);
        configuracion.addAnnotatedClass(Cliente.class);
        configuracion.addAnnotatedClass(Ventas.class);
        configuracion.addAnnotatedClass(Usuarios.class);

        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();
        sessionFactory = configuracion.buildSessionFactory(ssr);

        try {
            conexion = null;
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/cheetah", "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void desconectar() {
        if (sessionFactory != null && sessionFactory.isOpen()) {
            sessionFactory.close();
        }
    }

    //Usuario

    /**
     * Metodo para dar de alta a un Usuario
     *
     * @param nuevoUser Objeto de la clase Usuarios
     */
    public void altaUsuario(Usuarios nuevoUser) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoUser);
        sesion.getTransaction().commit();
        sesion.clear();
    }

    /**
     * Metodo para eliminar un Usuario
     *
     * @param usuarioEliminado Objeto de la clase Usuarios
     */
    public void eliminarUsuario(Usuarios usuarioEliminado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(usuarioEliminado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para modificar un Usuario
     *
     * @param usuarioSeleccion Objeto de la clase Usuarios
     */
    public void modificarUsuario(Usuarios usuarioSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(usuarioSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para coger los datos de los usuarios
     *
     * @return nos devolveran los datos de los usuarios
     */
    public ArrayList<Usuarios> getUsuario() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Usuarios");
        ArrayList<Usuarios> lista = (ArrayList<Usuarios>) query.getResultList();
        sesion.clear();
        return lista;
    }

    //Cliente

    /**
     * Metodo para dar de alta a un cliente
     *
     * @param nuevoCliente Objeto de la clase Cliente
     */
    public void altaCliente(Cliente nuevoCliente) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoCliente);
        sesion.getTransaction().commit();
        sesion.clear();
    }

    /**
     * Metodo para eliminar un cliente
     *
     * @param clienteEliminado Objeto de la clase Cliente
     */
    public void eliminarCliente(Cliente clienteEliminado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(clienteEliminado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para modificar un cliente
     *
     * @param clienteModificado Objeto de la clase Cliente
     */
    public void modificarCliente(Cliente clienteModificado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(clienteModificado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para coger los datos de los clientes
     *
     * @return nos devolveran los datos de los clientes
     */
    public ArrayList<Cliente> getCliente() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Cliente");
        ArrayList<Cliente> lista = (ArrayList<Cliente>) query.getResultList();
        sesion.clear();
        return lista;
    }

    //Vehiculo

    /**
     * Metodo para dar de alta a un vehiculo
     *
     * @param nuevoVehiculo Objeto de la clase Vehiculo
     */
    public void altaVehiculo(Vehiculo nuevoVehiculo) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoVehiculo);
        sesion.getTransaction().commit();
        sesion.clear();
    }

    /**
     * Metodo para eliminar un vehiculo
     *
     * @param vehiculoEliminado Objeto de la clase Vehiculo
     */
    public void eliminarVehiculo(Vehiculo vehiculoEliminado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(vehiculoEliminado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para modificar un vehiculo
     *
     * @param vehiculoModificado Objeto de la clase Vehiculo
     */
    public void modificarVehiculo(Vehiculo vehiculoModificado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(vehiculoModificado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para coger los datos de los vehiculos
     *
     * @return nos devolveran los datos de los vehiculos
     */
    public ArrayList<Vehiculo> getVehiculo() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Vehiculo");
        ArrayList<Vehiculo> lista = (ArrayList<Vehiculo>) query.getResultList();
        sesion.clear();
        return lista;
    }


    //Revision

    /**
     * Metodo para dar de alta a una revisión
     *
     * @param nuevaRevision Objeto de la clase Revision
     */
    public void altaRevision(Revision nuevaRevision) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevaRevision);
        sesion.getTransaction().commit();
        sesion.clear();
    }

    /**
     * Metodo para eliminar una revisión
     *
     * @param revisionEliminar Objeto de la clase Revision
     */
    public void eliminarRevision(Revision revisionEliminar) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(revisionEliminar);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para modificar una revisión
     *
     * @param revisionModificar Objeto de la clase Revision
     */
    public void modificarRevision(Revision revisionModificar) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(revisionModificar);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para coger los datos de las revisiones
     *
     * @return nos devolveran los datos de las revisiones
     */
    public ArrayList<Revision> getRevision() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Revision");
        ArrayList<Revision> lista = (ArrayList<Revision>) query.getResultList();
        sesion.clear();
        return lista;
    }

    //Venta

    /**
     * Metodo para dar de alta a una venta
     *
     * @param nuevaVenta Objeto de la clase Ventas
     */
    public void altaVenta(Ventas nuevaVenta) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevaVenta);
        sesion.getTransaction().commit();
        sesion.clear();
    }

    /**
     * Metodo para eliminar una venta
     *
     * @param ventaEliminar Objeto de la clase Ventas
     */
    public void eliminarVenta(Ventas ventaEliminar) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();

        sesion.delete(ventaEliminar);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para modificar una venta
     *
     * @param ventaModificar Objeto de la clase Ventas
     */
    public void modificarVenta(Ventas ventaModificar) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(ventaModificar);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para coger los datos de las ventas
     *
     * @return nos devolveran los datos de las ventas
     */
    public ArrayList<Ventas> getVentas() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Ventas");
        ArrayList<Ventas> lista = (ArrayList<Ventas>) query.getResultList();
        sesion.clear();
        return lista;
    }

    /**
     * Metodo con el cual podremos buscar a un cliente pasandole el dni como parametro
     *
     * @param dni Variable con la cual pasaremos el cliente a buscar
     * @return nos devolveran los datos de los clientes
     */
    public ArrayList<Cliente> getClientedni(String dni) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Cliente c where c.dni=:dni");
        query.setParameter("dni", dni);
        ArrayList<Cliente> lista = (ArrayList<Cliente>) query.getResultList();
        session.clear();
        return lista;
    }

    /**
     * Metodo con el cual podremos buscar un vehiculo pasandole la marca como parametro
     *
     * @param marca Variable con la cual pasaremos el vehiculo a buscar
     * @return nos devolveran los datos de los vehiculos
     */
    public ArrayList<Vehiculo> getVehiculoNombre(String marca) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Vehiculo v where v.marca=:marca");
        query.setParameter("marca", marca);
        ArrayList<Vehiculo> lista = (ArrayList<Vehiculo>) query.getResultList();
        session.clear();
        return lista;
    }

    /**
     * Metodo con el cual podremos buscar una venta pasandole el codigo de venta como parametro
     *
     * @param codVenta Variable con la cual pasaremos la venta a buscar
     * @return nos devolveran los datos de las ventas
     */
    public ArrayList<Ventas> getVentasCodVenta(int codVenta) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Ventas ve where ve.codVenta=:codVenta");
        query.setParameter("codVenta", codVenta);
        ArrayList<Ventas> lista = (ArrayList<Ventas>) query.getResultList();
        session.clear();
        return lista;
    }

    /**
     * Metodo con el cual añadiremos los cliente creados en una combobox para añadirlos en la venta
     *
     * @param lista ArrayList del cual obtendremos los datos del Cliente
     * @param combo Combobox en la que añadiremos los datos
     */
    public void setComboClienteVenta(ArrayList<Cliente> lista, JComboBox combo) {
        for (Cliente cliente : lista) {
            combo.addItem(cliente);
        }
        combo.setSelectedIndex(-1);
    }

    /**
     * Metodo con el cual añadiremos los cliente creados en una combobox para añadirlos en la venta
     *
     * @param lista ArrayList del cual obtendremos los datos del Cliente
     * @param combo Combobox en la que añadiremos los datos
     */
    public void setComboClienteVentaMod(ArrayList<Cliente> lista, JComboBox combo) {
        for (Cliente cliente : lista) {
            combo.addItem(cliente);
        }
        combo.setSelectedIndex(-1);
    }

    /**
     * Metodo con el cual añadiremos los vehiculos creados en una combobox para añadirlos en la revisión
     *
     * @param lista ArrayList del cual obtendremos los datos del Vehiculo
     * @param combo Combobox en la que añadiremos los datos
     */
    public void setComboVehiculoRevision(ArrayList<Vehiculo> lista, JComboBox combo) {
        for (Vehiculo vehiculo : lista) {
            combo.addItem(vehiculo);
        }
        combo.setSelectedIndex(-1);
    }

    /**
     * Metodo con el cual añadiremos los vehiculos creados en una combobox para añadirlos en la revisión
     *
     * @param lista ArrayList del cual obtendremos los datos del Vehiculo
     * @param combo Combobox en la que añadiremos los datos
     */
    public void setComboVehiculoRevisionMod(ArrayList<Vehiculo> lista, JComboBox combo) {
        for (Vehiculo vehiculo : lista) {
            combo.addItem(vehiculo);
        }
        combo.setSelectedIndex(-1);
    }

    /**
     * Metodo con el que comprobaremos el usuario y nos devolvera el tipo de permiso de este
     *
     * @param nombre     Variable mediante la cual pasamos el nombre de usuario
     * @param contraseña Variable mediante la cual pasamos la contraseña del usuario
     * @return el tipo de permiso que tiene el usuario pasado como parámetro
     */
    public String comprobarUsuario(String nombre, String contraseña) {
        String resultado = "SELECT permiso FROM usuarios WHERE nick ='" + nombre + "' AND passwd='" + contraseña + "'";
        String permisos = "";
        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(resultado);

            if (rs.next()) {
                permisos = rs.getString(1);

            } else {
                JOptionPane.showMessageDialog(null, "El usuario no existe", "Nombre de usuario existente", JOptionPane.ERROR_MESSAGE);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return permisos;
    }

    /**
     * Metodo con el cual comprobaremos si existen usuarios en la base de datos
     *
     * @return si existe el usuario
     * @throws SQLException
     */
    public Boolean existenUsuariosBBDD() throws SQLException {
        String resultado = "SELECT * FROM usuarios";
        Statement st = conexion.createStatement();
        ResultSet rs = st.executeQuery(resultado);
        if (rs.next()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Metodo con el cual comprobaremos mediante una función si existe el nick de usuario
     *
     * @param nombre Variable mediante la cual pasamos el nick del usuario
     * @return nos devolvera si existe ese nombre pasado como parámetro o no
     */
    public boolean existeUsuario(String nombre) {
        String userConsulta = "SELECT existeNombreUsuario(?)";
        PreparedStatement sentencia;
        boolean user = false;

        try {
            sentencia = conexion.prepareStatement(userConsulta);
            sentencia.setString(1, nombre);
            ResultSet resultSet = sentencia.executeQuery();
            resultSet.next();

            user = resultSet.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    /**
     * Metodo con el cual comprobaremos mediante una función si existe el cliente en la base de datos
     *
     * @param dni Variable mediante la cual pasamos el dni del usuario
     * @return nos devolvera si existe ese cliente pasado como parámetro o no
     */
    public boolean existeCliente(String dni) {
        String clientConsulta = "SELECT existeDniCliente(?)";
        PreparedStatement sentencia;
        boolean cliente = false;

        try {
            sentencia = conexion.prepareStatement(clientConsulta);
            sentencia.setString(1, dni);
            ResultSet resultSet = sentencia.executeQuery();
            resultSet.next();

            cliente = resultSet.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cliente;
    }

    /**
     * Metodo con el cual comprobaremos mediante una función si existe la venta en la base de datos
     *
     * @param codVenta Variable mediante la cual pasamos el codigo de venta
     * @return nos devolvera si existe esa venta pasada como parámetro o no
     */
    public boolean existeVenta(int codVenta) {
        String ventConsult = "SELECT existeCodVenta(?)";
        PreparedStatement sentencia;
        boolean venta = false;

        try {
            sentencia = conexion.prepareStatement(ventConsult);
            sentencia.setInt(1, codVenta);
            ResultSet resultSet = sentencia.executeQuery();
            resultSet.next();

            venta = resultSet.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return venta;
    }

}
