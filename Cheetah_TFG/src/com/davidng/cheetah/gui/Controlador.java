package com.davidng.cheetah.gui;

import cifrado.Cifrado;
import com.davidng.cheetah.*;
import com.davidng.cheetah.gui.CamposVacios.CamposVacios;
import com.davidng.cheetah.gui.Hash.Hash;
import com.davidng.cheetah.gui.Informes.InformesClient;
import com.davidng.cheetah.gui.Informes.InformesRevision;
import com.davidng.cheetah.gui.Informes.InformesVeh;
import com.davidng.cheetah.gui.Informes.InformesVent;
import com.davidng.cheetah.gui.LimpiarCampos.LimpiarCampos;
import com.davidng.cheetah.gui.Listados.Listados;
import com.davidng.cheetah.gui.Usuarios.NuevoUsuario;
import com.davidng.cheetah.gui.Usuarios.VistaUsuarios;
import com.davidng.cheetah.gui.Ventanas.Administrador;
import com.davidng.cheetah.gui.Ventanas.Carga.VentanaCarga;
import com.davidng.cheetah.gui.Ventanas.Clientes.InformesCliente;
import com.davidng.cheetah.gui.Ventanas.Clientes.ModificarCliente;
import com.davidng.cheetah.gui.Ventanas.Clientes.NuevoCliente;
import com.davidng.cheetah.gui.Ventanas.Clientes.VisualizarCliente;
import com.davidng.cheetah.gui.Ventanas.Revision.ModRevVeh;
import com.davidng.cheetah.gui.Ventanas.Revision.NuevRevVeh;
import com.davidng.cheetah.gui.Ventanas.Revision.VisualizarRevision;
import com.davidng.cheetah.gui.Ventanas.Usuarios.ModificarUsuario;
import com.davidng.cheetah.gui.Ventanas.Usuarios.NuevoUserAdmin;
import com.davidng.cheetah.gui.Ventanas.Usuarios.VisualizarUsuario;
import com.davidng.cheetah.gui.Ventanas.Vehiculos.InformesVehiculos;
import com.davidng.cheetah.gui.Ventanas.Vehiculos.ModificarVehiculo;
import com.davidng.cheetah.gui.Ventanas.Vehiculos.NuevoVehiculo;
import com.davidng.cheetah.gui.Ventanas.Vehiculos.VisualizarVeh;
import com.davidng.cheetah.gui.Ventanas.Ventas.InformeVenta;
import com.davidng.cheetah.gui.Ventanas.Ventas.ModificarVenta;
import com.davidng.cheetah.gui.Ventanas.Ventas.NuevaVenta;
import com.davidng.cheetah.gui.Ventanas.Ventas.VisualizarVenta;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.sql.SQLException;

/**
 * @author dnadeau
 * Clase donde crearemos el MVC con la llamada a las otras Clases
 */
public class Controlador implements ActionListener, ListSelectionListener {
    private Modelo modelo; //Objeto de la clase Modelo
    private VistaUsuarios usuario; //Objeto de la clase VistaUsuarios
    private NuevoUsuario nuevoUsuario; //Objeto de la clase NuevoUsuario
    private VentanaCarga ventanaCarga; //Objeto de la clase VentanaCarga
    //Administrador
    private Administrador administrador; //Objeto de la clase Administrador
    private ModificarUsuario modUser; //Objeto de la clase ModificarUsuario
    private NuevoUserAdmin nUserAdm; //Objeto de la clase NuevoUserAdmin
    private VisualizarUsuario visUser; //Objeto de la clase VisualizarUsuario
    //Cliente
    private NuevoCliente nClient; //Objeto de la clase NuevoCliente
    private ModificarCliente mClient; //Objeto de la clase ModificarCliente
    private InformesCliente informesCliente; //Objeto de la clase InformesCliente
    private VisualizarCliente visClien; //Objeto de la clase VisualizarCliente
    //Vehiculo
    private NuevoVehiculo nVehiculo; //Objeto de la clase NuevoVehiculo
    private ModificarVehiculo mVehiculo; //Objeto de la clase ModificarVehiculo
    private InformesVehiculos informesVehiculos; //Objeto de la clase InformesVehiculos
    private VisualizarVeh visVeh; //Objeto de la clase VisualizarVeh
    //Revision
    private NuevRevVeh nRevVeh; //Objeto de la clase NuevRevVeh
    private ModRevVeh modRevVeh; //Objeto de la clase ModRevVeh
    private VisualizarRevision visRev; //Objeto de la clase VisualizarRevision
    //Venta
    private NuevaVenta nVenta;  //Objeto de la clase NuevaVenta
    private ModificarVenta mVenta; //Objeto de la clase ModificarVenta
    private InformeVenta informeVenta; //Objeto de la clase InformeVenta
    private VisualizarVenta visVent; //Objeto de la clase VisualizarVenta

    private Listados list; //Objeto de la clase Listados
    private CamposVacios camposVacios; //Objeto de la clase CamposVacios
    private LimpiarCampos limpiarCampos; //Objeto de la clase LimpiarCampos

    /**
     * Constructor de la clase Controlador donde inicializamos los datos
     *
     * @param modelo       Objeto de la clase Modelo
     * @param ventanaCarga Objeto de la clase VentanaCarga
     */
    public Controlador(Modelo modelo, VentanaCarga ventanaCarga) {
        this.modelo = modelo;
        this.ventanaCarga = ventanaCarga;

        initClass();
        addActionListeners(this);
        addListSelectionListener(this);
        modelo.conectar();
        listar();

        try {
            if (modelo.existenUsuariosBBDD() == true) {
                usuario.setVisible(true);
            } else {
                nuevoUsuario.setVisible(true);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo donde llamamos a los metodos de listar que se encuentran en otra clase
     */
    private void listar() {
        list.listarUsuarios(modelo.getUsuario(), administrador);
        list.listarClientes(modelo.getCliente(), administrador);
        list.listarVehiculos(modelo.getVehiculo(), administrador);
        list.listarRevision(modelo.getRevision(), administrador);
        list.listarVentas(modelo.getVentas(), administrador);
    }

    /**
     * Metodo donde inicializamos los objetos creados anteriormente
     */
    private void initClass() {
        nuevoUsuario = new NuevoUsuario();
        administrador = new Administrador();

        modUser = new ModificarUsuario();
        nUserAdm = new NuevoUserAdmin();
        visUser = new VisualizarUsuario();

        nClient = new NuevoCliente();
        mClient = new ModificarCliente();
        informesCliente = new InformesCliente();
        visClien = new VisualizarCliente();

        nVehiculo = new NuevoVehiculo();
        mVehiculo = new ModificarVehiculo();
        informesVehiculos = new InformesVehiculos();
        visVeh = new VisualizarVeh();

        nRevVeh = new NuevRevVeh();
        modRevVeh = new ModRevVeh();
        visRev = new VisualizarRevision();
        nVenta = new NuevaVenta();
        mVenta = new ModificarVenta();
        informeVenta = new InformeVenta();
        visVent = new VisualizarVenta();

        list = new Listados();
        camposVacios = new CamposVacios();
        limpiarCampos = new LimpiarCampos();
        usuario = new VistaUsuarios();

    }

    /**
     * Metodo con el que daremos funcionalidad a los botones
     *
     * @param listeners
     */
    private void addActionListeners(ActionListener listeners) {
        //Botones VistaUsuario
        usuario.validarButton.addActionListener(listeners);

        //Botones NuevoUsuario
        nuevoUsuario.volverButton.addActionListener(listeners);
        nuevoUsuario.nuevoUserButton.addActionListener(listeners);

        //Botones Administrador Usuario
        administrador.eliminarUsuariosButton.addActionListener(listeners);
        administrador.modicarButton.addActionListener(listeners);
        administrador.crearUsuarioMod.addActionListener(listeners);
        administrador.visualizarDatosUserButton.addActionListener(listeners);
        visUser.atrasButton.addActionListener(listeners);

        //Botones Cliente
        administrador.crearClienteButton.addActionListener(listeners);
        administrador.eliminarClienteButton.addActionListener(listeners);
        administrador.modificarClienteButton.addActionListener(listeners);
        administrador.listarButton.addActionListener(listeners);
        administrador.informesClienteButton.addActionListener(listeners);
        administrador.visualizarDatosCliButton.addActionListener(listeners);
        visClien.atrasButton.addActionListener(listeners);
        administrador.buscarClienteButton.addActionListener(listeners);


        //Botones Vehiculo
        administrador.nuevoVehiculoButton.addActionListener(listeners);
        administrador.modificarVehiculoButton.addActionListener(listeners);
        administrador.eliminarVehiculoButton.addActionListener(listeners);
        administrador.informesVehiculoButton.addActionListener(listeners);
        administrador.visualizarDatosVehButton.addActionListener(listeners);
        visVeh.atrasButton.addActionListener(listeners);
        administrador.buscarVehiculoButton.addActionListener(listeners);

        //Botones Revision
        administrador.nuevaRevisionButton.addActionListener(listeners);
        administrador.modificarRevisionButton.addActionListener(listeners);
        administrador.eliminarRevisionButton.addActionListener(listeners);
        administrador.informesRevisionButton.addActionListener(listeners);
        administrador.visualizarDatosRevButton.addActionListener(listeners);
        visRev.atrasButton.addActionListener(listeners);

        //Botones Ventas
        administrador.crearVentaButton.addActionListener(listeners);
        administrador.modificarVentaButton.addActionListener(listeners);
        administrador.eliminarVentaButton.addActionListener(listeners);
        administrador.añadirVehVenButton.addActionListener(listeners);
        administrador.informesVentasButton.addActionListener(listeners);
        administrador.visualizarDatosVenButton.addActionListener(listeners);
        administrador.facturaVentaButton.addActionListener(listeners);
        administrador.buscarVentaButton.addActionListener(listeners);
        visVent.atrasButton.addActionListener(listeners);

        //Botones ModificarUsuario
        modUser.guardarUserModButton.addActionListener(listeners);
        modUser.atrasModUserButton.addActionListener(listeners);

        //Botones NuevoUserAdmin
        nUserAdm.nuevoUserButton.addActionListener(listeners);
        nUserAdm.atrasUserButton.addActionListener(listeners);

        //Botones NuevoCliente
        nClient.atrasClientButton.addActionListener(listeners);
        nClient.nuevoClienteButton.addActionListener(listeners);

        //Botones ModificarCliente
        mClient.atrasModClienButton.addActionListener(listeners);
        mClient.guardarClientButton.addActionListener(listeners);

        //Botones NuevoVehiculo
        nVehiculo.atrasVehButton.addActionListener(listeners);
        nVehiculo.nuevoVehButton.addActionListener(listeners);

        //Botones ModificarVehiculo
        mVehiculo.atrasModVehButton.addActionListener(listeners);
        mVehiculo.guardarVehModButton.addActionListener(listeners);

        //Botones ModificarRevision
        modRevVeh.atrasRevButton.addActionListener(listeners);
        modRevVeh.guardarRevButton.addActionListener(listeners);

        //Botones NuevaRevision
        nRevVeh.atrasRevNvButton.addActionListener(listeners);
        nRevVeh.nuevoRevButton.addActionListener(listeners);

        //Botones NuevaVenta
        nVenta.atrasVentaButton.addActionListener(listeners);
        nVenta.nuevoventaButton.addActionListener(listeners);

        //Botones ModificarVenta
        mVenta.atrasModVenButton.addActionListener(listeners);
        mVenta.guardarModVenButton.addActionListener(listeners);

        //Informes Clientes
        informesCliente.informeButton.addActionListener(listeners);
        informesCliente.busquedaButton.addActionListener(listeners);
        //Informes Vehiculos
        informesVehiculos.informeTotal.addActionListener(listeners);
        informesVehiculos.informesGrafico.addActionListener(listeners);
        //Informes Ventas
        informeVenta.InformeTotal.addActionListener(listeners);
        informeVenta.InformeGrafico.addActionListener(listeners);
    }

    /**
     * Metodo con el que daremos funcionalidad a los Default List Model
     *
     * @param listener
     */
    private void addListSelectionListener(ListSelectionListener listener) {
        administrador.listaUsuarios.addListSelectionListener(listener);
        administrador.listaClientes.addListSelectionListener(listener);
        administrador.listaVehiculosAdmin.addListSelectionListener(listener);
        administrador.listaRevision.addListSelectionListener(listener);
        administrador.listaVenta.addListSelectionListener(listener);
        administrador.listaVentaVehiculo.addListSelectionListener(listener);
    }

    /**
     * Metodo donde le diremos a cada boton la función que tiene que hacer
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {


            case "InformesCliente":
                administrador.setVisible(true);
                informesCliente.setVisible(true);
                break;

            case "InformesVehiculo":
                administrador.setVisible(true);
                informesVehiculos.setVisible(true);
                break;


            case "InformesVenta":
                administrador.setVisible(true);
                informeVenta.setVisible(true);
                break;


            case "AtrasModUser":
                modUser.setVisible(false);
                modUser.dispose();
                administrador.setVisible(true);
                break;

            case "Atras":
                usuario.setVisible(true);
                nuevoUsuario.setVisible(false);
                nuevoUsuario.dispose();
                break;

            case "ModificarUsuarioAdm":
                if (administrador.listaUsuarios.getSelectedValue() != null) {
                    administrador.setVisible(true);
                    administrador.dispose();
                    modUser.setVisible(true);
                    modUser.txtContrMod.setEnabled(false);
                } else {
                    JOptionPane.showMessageDialog(null, "No has seleccionado un usuario", "Ninguna selección", JOptionPane.WARNING_MESSAGE);
                }
                break;

            case "CrearUsuarioAdm":
                administrador.setVisible(false);
                administrador.dispose();
                nUserAdm.setVisible(true);
                break;

            case "AtrasNuevUser":
                nUserAdm.setVisible(false);
                nUserAdm.dispose();
                administrador.setVisible(true);
                break;

            case "AtrasVisUser":
                visUser.setVisible(false);
                visUser.dispose();
                administrador.setVisible(true);
                break;

            //CLIENTES

            case "CrearClienteAdmin":
                administrador.setVisible(false);
                administrador.dispose();
                nClient.setVisible(true);
                break;

            case "AtrasClienteAdmin":
                nClient.setVisible(false);
                nClient.dispose();
                administrador.setVisible(true);
                break;

            case "ModificarClientAdmin":
                if (administrador.listaClientes.getSelectedValue() != null) {
                    administrador.setVisible(false);
                    administrador.dispose();
                    mClient.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(null, "No has seleccionado un cliente", "Ninguna selección", JOptionPane.WARNING_MESSAGE);
                }
                break;

            case "AtrasModClient":
                mClient.setVisible(false);
                mClient.dispose();
                administrador.setVisible(true);
                break;

            case "AtrasVisCliente":
                administrador.setVisible(true);
                visClien.setVisible(false);
                visClien.dispose();
                break;

            //VEHICULOS

            case "NuevoVehiculoAdmin":
                administrador.setVisible(false);
                administrador.dispose();
                nVehiculo.setVisible(true);
                break;

            case "AtrasVehiculoAdmin":
                administrador.setVisible(true);
                nVehiculo.setVisible(false);
                nVehiculo.dispose();
                break;

            case "AtrasModAdmVeh":
                administrador.setVisible(true);
                mVehiculo.setVisible(false);
                mVehiculo.dispose();
                break;

            case "ModificarVehAdmin":
                if (administrador.listaVehiculosAdmin.getSelectedValue() != null) {
                    administrador.setVisible(false);
                    administrador.dispose();
                    mVehiculo.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(null, "No has seleccionado un vehiculo", "Ninguna selección", JOptionPane.WARNING_MESSAGE);
                }
                break;

            case "AtrasVisVeh":
                administrador.setVisible(true);
                visVeh.setVisible(false);
                visVeh.dispose();
                break;

            //REVISIONES

            case "AtrasModRevAdm":
                administrador.setVisible(true);
                modRevVeh.setVisible(false);
                modRevVeh.dispose();
                break;

            case "NuevoRevVehAdm":
                nRevVeh.setVisible(true);
                administrador.setVisible(false);
                administrador.dispose();
                break;

            case "ModificarRevVehAdm":
                if (administrador.listaRevision.getSelectedValue() != null) {
                    modRevVeh.setVisible(true);
                    administrador.setVisible(false);
                    administrador.dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "No has seleccionado una revisiónn", "Ninguna selección", JOptionPane.WARNING_MESSAGE);
                }
                break;

            case "AtrasRevAdm":
                nRevVeh.setVisible(false);
                nRevVeh.dispose();
                administrador.setVisible(true);
                break;

            case "AtrasVisRevision":
                administrador.setVisible(true);
                visRev.setVisible(false);
                visRev.dispose();
                break;

            //VENTAS

            case "CrearVentaAdm":
                administrador.setVisible(false);
                administrador.dispose();
                nVenta.setVisible(true);
                break;

            case "ModificarVentaAdm":
                if (administrador.listaVenta.getSelectedValue() != null) {
                    administrador.setVisible(false);
                    administrador.dispose();
                    mVenta.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(null, "No has seleccionado una venta", "Ninguna selección", JOptionPane.WARNING_MESSAGE);
                }
                break;

            case "AtrasNuvVenAdm":
                nVenta.setVisible(false);
                nVenta.dispose();
                administrador.setVisible(true);
                break;

            case "AtrasModVentaAdm":
                mVenta.setVisible(false);
                mVenta.dispose();
                administrador.setVisible(true);
                break;

            case "AtrasVisVen":
                visVent.setVisible(false);
                visVent.dispose();
                administrador.setVisible(true);
                break;

            case "Validar":
                if (camposVacios.camposVaciosLogin(usuario)) {
                    JOptionPane.showMessageDialog(null, "Existen campos vacios", "Campos Vacios", JOptionPane.WARNING_MESSAGE);
                } else {
                    String permisos = modelo.comprobarUsuario(usuario.txtNombreUsuario.getText(), Hash.sha1(usuario.txtContraseña.getText()));
                    if (permisos.equalsIgnoreCase("Administrador")) {
                        administrador.setVisible(true);
                        usuario.setVisible(false);
                        usuario.dispose();

                    } else if (permisos.equalsIgnoreCase("Vendedor")) {
                        administrador.setVisible(true);
                        usuario.setVisible(false);
                        usuario.dispose();
                        administrador.listaUsuarios.setEnabled(false);
                        administrador.crearUsuarioMod.setEnabled(false);
                        administrador.modicarButton.setEnabled(false);
                        administrador.eliminarUsuariosButton.setEnabled(false);
                        administrador.visualizarDatosUserButton.setEnabled(false);

                    } else if (permisos.equalsIgnoreCase("Jefe")) {
                        administrador.setVisible(true);
                        usuario.setVisible(false);
                        usuario.dispose();

                        administrador.listaUsuarios.setEnabled(false);
                        administrador.crearUsuarioMod.setEnabled(false);
                        administrador.modicarButton.setEnabled(false);
                        administrador.eliminarUsuariosButton.setEnabled(false);
                        administrador.visualizarDatosUserButton.setEnabled(false);

                        administrador.listaVehiculosAdmin.setEnabled(false);
                        administrador.listaVehiculoVenta.setEnabled(false);
                        administrador.nuevoVehiculoButton.setEnabled(false);
                        administrador.eliminarVehiculoButton.setEnabled(false);
                        administrador.modificarVehiculoButton.setEnabled(false);
                        administrador.listarButton.setEnabled(false);
                        administrador.visualizarDatosVehButton.setEnabled(false);

                        administrador.listaRevision.setEnabled(false);
                        administrador.nuevaRevisionButton.setEnabled(false);
                        administrador.modificarRevisionButton.setEnabled(false);
                        administrador.eliminarRevisionButton.setEnabled(false);
                        administrador.visualizarDatosRevButton.setEnabled(false);

                        administrador.listaClientes.setEnabled(false);
                        administrador.crearClienteButton.setEnabled(false);
                        administrador.eliminarClienteButton.setEnabled(false);
                        administrador.modificarClienteButton.setEnabled(false);
                        administrador.visualizarDatosCliButton.setEnabled(false);

                        administrador.listaVenta.setEnabled(false);
                        administrador.listaVentaVehiculo.setEnabled(false);
                        administrador.crearVentaButton.setEnabled(false);
                        administrador.eliminarVentaButton.setEnabled(false);
                        administrador.modificarVentaButton.setEnabled(false);
                        administrador.añadirVehVenButton.setEnabled(false);
                        administrador.facturaVentaButton.setEnabled(false);
                        administrador.visualizarDatosVenButton.setEnabled(false);
                    }
                }
                limpiarCampos.limpiarCamposLogin(usuario);
                break;

            case "Nuevo":
                if (camposVacios.camposVacios(nuevoUsuario)) {
                    JOptionPane.showMessageDialog(null, "Existen campos vacios", "Campos Vacios", JOptionPane.WARNING_MESSAGE);
                } else if (modelo.existeUsuario(nuevoUsuario.txtNickUsuario.getText())) {
                    JOptionPane.showMessageDialog(null, "Existe el nick de usuario", "Nombre de nick existente", JOptionPane.ERROR_MESSAGE);
                } else if (!camposVacios.comprobanteEmail(nuevoUsuario.txtEmailUsuario.getText())) {
                    JOptionPane.showMessageDialog(null, "Error en el correo electronico", "Error Email", JOptionPane.WARNING_MESSAGE);
                } else {
                    Usuarios nuevoUser = new Usuarios();
                    nuevoUser.setNombre(nuevoUsuario.txtNombreUsuario.getText());
                    nuevoUser.setApellidos(nuevoUsuario.txtApellidoUsuario.getText());
                    nuevoUser.setEmail(nuevoUsuario.txtEmailUsuario.getText());
                    nuevoUser.setFechaNacimiento(Date.valueOf(nuevoUsuario.fechaNacUser.getDate()));
                    nuevoUser.setNick(nuevoUsuario.txtNickUsuario.getText());
                    nuevoUser.setPasswd(Hash.sha1(nuevoUsuario.txtNuevaContraseña.getText()));
                    nuevoUser.setPermiso(String.valueOf(nuevoUsuario.cbPermisoUser.getSelectedItem()));
                    modelo.altaUsuario(nuevoUser);
                    JOptionPane.showMessageDialog(null, "Nuevo usuario añadido", "Usuario añadido", JOptionPane.INFORMATION_MESSAGE);
                }
                limpiarCampos.limpiarCamposRegistro(nuevoUsuario);
                list.listarUsuarios(modelo.getUsuario(), administrador);
                break;

            case "GuardarUsuarioModificado":
                if (camposVacios.camposVaciosModificarUser(modUser)) {
                    JOptionPane.showMessageDialog(null, "Existen campos vacios", "Campos Vacios", JOptionPane.WARNING_MESSAGE);
                } else if (!camposVacios.comprobanteEmail(modUser.txtEmailMod.getText())) {
                    JOptionPane.showMessageDialog(null, "Error en el correo electronico", "Error Email", JOptionPane.WARNING_MESSAGE);
                } else {
                    Usuarios usuarioModificar = (Usuarios) administrador.listaUsuarios.getSelectedValue();
                    usuarioModificar.setNombre(modUser.txtNombUserMod.getText());
                    usuarioModificar.setApellidos(modUser.txtApellMod.getText());
                    usuarioModificar.setEmail(modUser.txtEmailMod.getText());
                    usuarioModificar.setFechaNacimiento(Date.valueOf(modUser.fechaNacModUser.getDate()));
                    usuarioModificar.setNick(modUser.txtNickMod.getText());
                    usuarioModificar.setPasswd(modUser.txtContrMod.getText());
                    usuarioModificar.setPermiso(String.valueOf(modUser.cbPermMod.getSelectedItem()));
                    modelo.modificarUsuario(usuarioModificar);
                    JOptionPane.showMessageDialog(null, "Usuario modificado", "Usuario modificado", JOptionPane.INFORMATION_MESSAGE);
                }
                limpiarCampos.limpiarCamposModificarUser(modUser);
                list.listarUsuarios(modelo.getUsuario(), administrador);
                break;

            case "NuevoUsuarioAdmin":
                if (camposVacios.camposVaciosNuevUserAdmin(nUserAdm)) {
                    JOptionPane.showMessageDialog(null, "Existen campos vacios", "Campos Vacios", JOptionPane.WARNING_MESSAGE);
                } else if (modelo.existeUsuario(nUserAdm.txtNickUserNuev.getText())) {
                    JOptionPane.showMessageDialog(null, "Existe el nick de usuario", "Nombre de nick existente", JOptionPane.ERROR_MESSAGE);
                } else if (!camposVacios.comprobanteEmail(nUserAdm.txtEmaiUserNuev.getText())) {
                    JOptionPane.showMessageDialog(null, "Error en el correo electronico", "Error Email", JOptionPane.WARNING_MESSAGE);
                } else {
                    Usuarios nuevoUser = new Usuarios();
                    nuevoUser.setNombre(nUserAdm.txtNomUserNuev.getText());
                    nuevoUser.setApellidos(nUserAdm.txtApeUserNuev.getText());
                    nuevoUser.setEmail(nUserAdm.txtEmaiUserNuev.getText());
                    nuevoUser.setFechaNacimiento(Date.valueOf(nUserAdm.fechaNacNuevUser.getDate()));
                    nuevoUser.setNick(nUserAdm.txtNickUserNuev.getText());
                    nuevoUser.setPasswd(Hash.sha1(nUserAdm.txtPasswdUserNuev.getText()));
                    nuevoUser.setPermiso(String.valueOf(nUserAdm.cbPermUserNuev.getSelectedItem()));
                    modelo.altaUsuario(nuevoUser);
                    JOptionPane.showMessageDialog(null, "Nuevo usuario añadido", "Usuario añadido", JOptionPane.INFORMATION_MESSAGE);
                    limpiarCampos.limpiarCamposNuevoUser(nUserAdm);
                }

                list.listarUsuarios(modelo.getUsuario(), administrador);
                break;

            case "EliminarUsuario":
                if (administrador.listaUsuarios.getSelectedValue() != null) {
                    if (JOptionPane.showConfirmDialog(null, "¿Estas seguro que quieres eliminar este usuario?", "Eliminar Usuario", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                        Usuarios usuarioEliminar = (Usuarios) administrador.listaUsuarios.getSelectedValue();
                        modelo.eliminarUsuario(usuarioEliminar);
                    } else {
                        JOptionPane.showMessageDialog(null, "El usuario no ha sido eliminado", "Usuario", JOptionPane.INFORMATION_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No has seleccionado un usuario", "Ninguna selección", JOptionPane.WARNING_MESSAGE);
                }
                list.listarUsuarios(modelo.getUsuario(), administrador);
                break;

            case "VisualizarDatosUser":
                if (administrador.listaUsuarios.getSelectedValue() != null) {
                    visUser.setVisible(true);
                    administrador.setVisible(false);
                    administrador.dispose();
                    Usuarios usuarioVisualizar = (Usuarios) administrador.listaUsuarios.getSelectedValue();
                    visUser.txtNomUser.setText(usuarioVisualizar.getNombre());
                    visUser.txtApell.setText(usuarioVisualizar.getApellidos());
                    visUser.txtEmail.setText(usuarioVisualizar.getEmail());
                    visUser.fechaNac.setDate(usuarioVisualizar.getFechaNacimiento().toLocalDate());
                    visUser.txtNick.setText(usuarioVisualizar.getNick());
                    visUser.txtPasswd.setText(usuarioVisualizar.getPasswd());
                    visUser.cbPermiso.setSelectedItem(usuarioVisualizar.getPermiso());
                } else {
                    JOptionPane.showMessageDialog(null, "No has seleccionado un usuario", "Ninguna selección", JOptionPane.WARNING_MESSAGE);
                }
                break;


//CLIENTE
            case "NuevoClienteAdmin":
                if (camposVacios.camposVaciosNuevClient(nClient)) {
                    JOptionPane.showMessageDialog(null, "Existen campos vacios", "Campos Vacios", JOptionPane.WARNING_MESSAGE);
                } else {
                    try {
                        if (modelo.existeCliente(nClient.txtDniClient.getText())) {
                            JOptionPane.showMessageDialog(null, "Existe el cliente", "Cliente existente", JOptionPane.ERROR_MESSAGE);
                        } else if (!isNumeric(nClient.txtTelefClien.getText()) || !isNumeric(nClient.txtCPClien.getText())) {
                            JOptionPane.showMessageDialog(null, "Debes introducir numeros", "Error", JOptionPane.ERROR_MESSAGE);
                        } else if (!camposVacios.comprobanteDni(nClient.txtDniClient.getText())) {
                            JOptionPane.showMessageDialog(null, "DNI mal escrito", "Error", JOptionPane.WARNING_MESSAGE);
                        } else if (!camposVacios.comprobanteEmail(nClient.txtEmailClien.getText())) {
                            JOptionPane.showMessageDialog(null, "Error en el correo electronico", "Error Email", JOptionPane.WARNING_MESSAGE);
                        } else {
                            Cliente nCliente = new Cliente();
                            nCliente.setNombre(nClient.txtNomClien.getText());
                            nCliente.setApellidos(nClient.txtApeClien.getText());

                            nCliente.setDni(nClient.txtDniClient.getText());
                            nCliente.setFechaNacimiento(Date.valueOf(nClient.fechaNacClien.getDate()));
                            nCliente.setEmail(Cifrado.encriptar(nClient.txtEmailClien.getText(), "cheetah2"));
                            nCliente.setTelefono(Integer.parseInt(nClient.txtTelefClien.getText()));
                            nCliente.setDireccion(Cifrado.encriptar(nClient.txtDireccClien.getText(), "cheetah"));
                            nCliente.setCodPostal(Integer.parseInt(nClient.txtCPClien.getText()));
                            modelo.altaCliente(nCliente);
                            JOptionPane.showMessageDialog(null, "Nuevo Cliente añadido", "Cliente añadido", JOptionPane.INFORMATION_MESSAGE);
                            list.listarClientes(modelo.getCliente(), administrador);
                            limpiarCampos.limpiarCamposNuevClient(nClient);
                        }
                    } catch (UnsupportedEncodingException ex) {
                        ex.printStackTrace();
                    } catch (NoSuchAlgorithmException ex) {
                        ex.printStackTrace();
                    } catch (InvalidKeyException ex) {
                        ex.printStackTrace();
                    } catch (NoSuchPaddingException ex) {
                        ex.printStackTrace();
                    } catch (IllegalBlockSizeException ex) {
                        ex.printStackTrace();
                    } catch (BadPaddingException ex) {
                        ex.printStackTrace();
                    }
                }

                break;

            case "GuardarClientAdmin":
                if (camposVacios.camposVaciosModClient(mClient)) {
                    JOptionPane.showMessageDialog(null, "Existen campos vacios", "Campos Vacios", JOptionPane.WARNING_MESSAGE);
                } else if (!isNumeric(mClient.txtTelClienMod.getText()) || !isNumeric(mClient.txtCPClienMod.getText())) {
                    JOptionPane.showMessageDialog(null, "Debes introducir numeros", "Error", JOptionPane.ERROR_MESSAGE);
                } else if (!camposVacios.comprobanteDni(mClient.txtDniClienMod.getText())) {
                    JOptionPane.showMessageDialog(null, "DNI mal escrito", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    Cliente nCliente = (Cliente) administrador.listaClientes.getSelectedValue();
                    try {
                        nCliente.setNombre(mClient.txtNombreClienMod.getText());
                        nCliente.setApellidos(mClient.txtApeClienMod.getText());
                        nCliente.setDni(mClient.txtDniClienMod.getText());
                        nCliente.setFechaNacimiento(Date.valueOf(mClient.FechaNacClienMod.getDate()));
                        nCliente.setEmail(Cifrado.encriptar(mClient.txtEmailClienMod.getText(), "cheetah2"));
                        nCliente.setTelefono(Integer.parseInt(mClient.txtTelClienMod.getText()));

                        nCliente.setDireccion(Cifrado.encriptar(mClient.txtDireCienMod.getText(), "cheetah"));
                    } catch (UnsupportedEncodingException ex) {
                        ex.printStackTrace();
                    } catch (NoSuchAlgorithmException ex) {
                        ex.printStackTrace();
                    } catch (InvalidKeyException ex) {
                        ex.printStackTrace();
                    } catch (NoSuchPaddingException ex) {
                        ex.printStackTrace();
                    } catch (IllegalBlockSizeException ex) {
                        ex.printStackTrace();
                    } catch (BadPaddingException ex) {
                        ex.printStackTrace();
                    }
                    nCliente.setCodPostal(Integer.parseInt(mClient.txtCPClienMod.getText()));
                    modelo.modificarCliente(nCliente);
                    JOptionPane.showMessageDialog(null, "Cliente Modificado", "Cliente Modificado", JOptionPane.INFORMATION_MESSAGE);
                    list.listarClientes(modelo.getCliente(), administrador);
                }
                limpiarCampos.limpiarCamposModClient(mClient);
                break;

            case "EliminarClienteAdmin":
                if (administrador.listaClientes.getSelectedValue() != null) {
                    if (JOptionPane.showConfirmDialog(null, "¿Estas seguro que quieres eliminar este cliente?", "Eliminar Cliente", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                        Cliente clienteEliminar = (Cliente) administrador.listaClientes.getSelectedValue();
                        modelo.eliminarCliente(clienteEliminar);

                    } else {
                        JOptionPane.showMessageDialog(null, "El cliente no ha sido eliminado", "Cliente", JOptionPane.INFORMATION_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No has seleccionado un cliente", "Ninguna selección", JOptionPane.WARNING_MESSAGE);
                }
                list.listarClientes(modelo.getCliente(), administrador);
                list.listarVentas(modelo.getVentas(), administrador);
                break;

            case "VisualizarDatosCliente":
                if (administrador.listaClientes.getSelectedValue() != null) {
                    administrador.setVisible(false);
                    administrador.dispose();
                    visClien.setVisible(true);

                    Cliente clienteVisualizar = (Cliente) administrador.listaClientes.getSelectedValue();
                    try {
                        visClien.txtNombre.setText(clienteVisualizar.getNombre());
                        visClien.txtApellidos.setText(clienteVisualizar.getApellidos());
                        visClien.txtDNI.setText(clienteVisualizar.getDni());
                        visClien.fechaNacimiento.setDate(clienteVisualizar.getFechaNacimiento().toLocalDate());
                        visClien.txtEmail.setText(Cifrado.desencriptar(clienteVisualizar.getEmail(), "cheetah2"));
                        visClien.txtTelefono.setText(String.valueOf(clienteVisualizar.getTelefono()));
                        visClien.txtDireccion.setText(Cifrado.desencriptar(clienteVisualizar.getDireccion(), "cheetah"));
                        visClien.txtCP.setText(String.valueOf(clienteVisualizar.getCodPostal()));
                    } catch (UnsupportedEncodingException ex) {
                        ex.printStackTrace();
                    } catch (NoSuchAlgorithmException ex) {
                        ex.printStackTrace();
                    } catch (InvalidKeyException ex) {
                        ex.printStackTrace();
                    } catch (NoSuchPaddingException ex) {
                        ex.printStackTrace();
                    } catch (IllegalBlockSizeException ex) {
                        ex.printStackTrace();
                    } catch (BadPaddingException ex) {
                        ex.printStackTrace();
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "No has seleccionado un cliente", "Ninguna selección", JOptionPane.WARNING_MESSAGE);
                }

                break;

            case "BuscarCliente":
                String dniBuscador = administrador.txtBuscarCliente.getText();

                administrador.txtBuscarCliente.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyTyped(KeyEvent keyEvent) {
                        if (administrador.txtBuscarCliente.getText().length() == 0) {
                            list.listarClientes(modelo.getCliente(), administrador);
                        }
                    }
                });

                if (!dniBuscador.isEmpty()) {
                    list.listarClientes(modelo.getClientedni(dniBuscador), administrador);
                }
                break;

            case "GenerarInformeCliente":
                InformesClient informesClient = new InformesClient();
                informesClient.informClientCompleto(modelo);
                break;

            case "BuscarClienteInforme":
                InformesClient informesClientBusq = new InformesClient();
                String dniBuscar = JOptionPane.showInputDialog("Introduce un dni: ");
                informesClientBusq.informClientBusqueda(modelo, dniBuscar);
                break;

//REVISION
            case "NuevoRevAdm":
                if (camposVacios.camposVaciosNuvRev(nRevVeh)) {
                    JOptionPane.showMessageDialog(null, "Existen campos vacios", "Campos Vacios", JOptionPane.WARNING_MESSAGE);
                } else {
                    Revision revision = new Revision();
                    revision.setCambioRadiador(Date.valueOf(nRevVeh.fechaRadiador.getDate()));
                    revision.setCambioAceite(Date.valueOf(nRevVeh.fechaAceite.getDate()));
                    revision.setCambioFiltro(Date.valueOf(nRevVeh.fechaFiltro.getDate()));
                    revision.setCambioFrenos(Date.valueOf(nRevVeh.fechaFrenos.getDate()));
                    revision.setCambioNeumaticos(Date.valueOf(nRevVeh.fechaNeumaticos.getDate()));
                    revision.setCambioLuces(Date.valueOf(nRevVeh.fechaLuces.getDate()));
                    revision.setCambioAmortiguador(Date.valueOf(nRevVeh.fechaAmortiguador.getDate()));
                    revision.setVehiculo((Vehiculo) nRevVeh.cbVehiculosRevision.getSelectedItem());
                    modelo.altaRevision(revision);
                    JOptionPane.showMessageDialog(null, "Nueva revisión añadido", "Revisión añadida", JOptionPane.INFORMATION_MESSAGE);
                    list.listarRevision(modelo.getRevision(), administrador);
                }
                limpiarCampos.limpiarCamposRev(nRevVeh);
                break;

            case "GuardarRevAdm":
                if (camposVacios.camposVaciosModRev(modRevVeh)) {
                    JOptionPane.showMessageDialog(null, "Existen campos vacios", "Campos Vacios", JOptionPane.WARNING_MESSAGE);
                } else {
                    Revision revisionModificar = (Revision) administrador.listaRevision.getSelectedValue();
                    revisionModificar.setCambioRadiador(Date.valueOf(modRevVeh.fechaRadiador.getDate()));
                    revisionModificar.setCambioAceite(Date.valueOf(modRevVeh.fechaAceite.getDate()));
                    revisionModificar.setCambioFiltro(Date.valueOf(modRevVeh.fechaFiltro.getDate()));
                    revisionModificar.setCambioFrenos(Date.valueOf(modRevVeh.fechaFreno.getDate()));
                    revisionModificar.setCambioNeumaticos(Date.valueOf(modRevVeh.fechaNeumatico.getDate()));
                    revisionModificar.setCambioLuces(Date.valueOf(modRevVeh.fechaLuces.getDate()));
                    revisionModificar.setCambioAmortiguador(Date.valueOf(modRevVeh.fechaAmortiguador.getDate()));
                    revisionModificar.setVehiculo((Vehiculo) modRevVeh.cbVehiculoRevisionMod.getSelectedItem());
                    modelo.modificarRevision(revisionModificar);
                    JOptionPane.showMessageDialog(null, "Revisión Modificada", "Revisión Modificada", JOptionPane.INFORMATION_MESSAGE);
                    list.listarRevision(modelo.getRevision(), administrador);
                }
                limpiarCampos.limpiarCamposModRev(modRevVeh);
                break;

            case "EliminarRevVehAdm":
                if (administrador.listaRevision.getSelectedValue() != null) {
                    if (JOptionPane.showConfirmDialog(null, "¿Estas seguro que quieres eliminar este cliente?", "Eliminar Cliente", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                        Revision revisionEliminar = (Revision) administrador.listaRevision.getSelectedValue();
                        modelo.eliminarRevision(revisionEliminar);
                    } else {
                        JOptionPane.showMessageDialog(null, "La revisión no ha sido eliminada", "Revisión", JOptionPane.INFORMATION_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Revision no seleccionada", "No seleccion", JOptionPane.WARNING_MESSAGE);
                }
                list.listarRevision(modelo.getRevision(), administrador);
                break;

            case "VisualizarDatosRev":
                if (administrador.listaRevision.getSelectedValue() != null) {
                    visRev.setVisible(true);
                    administrador.setVisible(false);
                    administrador.dispose();
                    Revision revisionVisualizar = (Revision) administrador.listaRevision.getSelectedValue();
                    visRev.fechaRadiador.setDate(revisionVisualizar.getCambioRadiador().toLocalDate());
                    visRev.fechaNeumatico.setDate(revisionVisualizar.getCambioNeumaticos().toLocalDate());
                    visRev.fechaFrenos.setDate(revisionVisualizar.getCambioFrenos().toLocalDate());
                    visRev.fechaAceite.setDate(revisionVisualizar.getCambioAceite().toLocalDate());
                    visRev.fechaAmortiguador.setDate(revisionVisualizar.getCambioAmortiguador().toLocalDate());
                    visRev.fechaFiltro.setDate(revisionVisualizar.getCambioFiltro().toLocalDate());
                    visRev.fechaLuces.setDate(revisionVisualizar.getCambioLuces().toLocalDate());
                } else {
                    JOptionPane.showMessageDialog(null, "Revision no seleccionada", "No seleccion", JOptionPane.WARNING_MESSAGE);
                }

                break;

            case "InformesRevision":
                InformesRevision informesRevision = new InformesRevision();
                informesRevision.informRevCompleto(modelo);
                break;
//VEHICULO
            case "CrearVehiculoAdmin":
                if (camposVacios.camposVaciosVeh(nVehiculo)) {
                    JOptionPane.showMessageDialog(null, "Existen campos vacios", "Campos Vacios", JOptionPane.WARNING_MESSAGE);
                } else if (!isNumeric(nVehiculo.txtPotencia.getText())) {
                    JOptionPane.showMessageDialog(null, "Debes introducir numeros", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    Vehiculo vehiculo = new Vehiculo();
                    vehiculo.setMarca(String.valueOf(nVehiculo.cbMarcaVeh.getSelectedItem()));
                    vehiculo.setModelo(nVehiculo.txtModeloVeh.getText());
                    vehiculo.setTipoVehiculo(String.valueOf(nVehiculo.cbTipoVeh.getSelectedItem()));
                    vehiculo.setDomicilioVeh(nVehiculo.txtDomicilio.getText());
                    vehiculo.setMotor(String.valueOf(nVehiculo.cbMotor.getSelectedItem()));
                    vehiculo.setPotencia(Integer.parseInt(nVehiculo.txtPotencia.getText()));
                    vehiculo.setColor(nVehiculo.txtColor.getText());
                    vehiculo.setFechaFabricacion(Date.valueOf(nVehiculo.fechaFabriVeh.getDate()));
                    modelo.altaVehiculo(vehiculo);
                    JOptionPane.showMessageDialog(null, "Nuevo Vehiculo añadido", "Vehiculo añadido", JOptionPane.INFORMATION_MESSAGE);
                    list.listarVehiculos(modelo.getVehiculo(), administrador);
                }
                limpiarCampos.limpiarCamposVeh(nVehiculo);
                break;

            case "GuardarVehModAdm":
                if (camposVacios.camposVaciosModVeh(mVehiculo)) {
                    JOptionPane.showMessageDialog(null, "Existen campos vacios", "Campos Vacios", JOptionPane.WARNING_MESSAGE);
                } else if (!isNumeric(mVehiculo.txtPotencia.getText())) {
                    JOptionPane.showMessageDialog(null, "Debes introducir numeros", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    Vehiculo vehiculoMod = (Vehiculo) administrador.listaVehiculosAdmin.getSelectedValue();
                    vehiculoMod.setMarca(String.valueOf(mVehiculo.cbModVeh.getSelectedItem()));
                    vehiculoMod.setModelo(mVehiculo.txtModelo.getText());
                    vehiculoMod.setTipoVehiculo(String.valueOf(mVehiculo.cbTipoVeh.getSelectedItem()));
                    vehiculoMod.setDomicilioVeh(mVehiculo.txtDomVeh.getText());
                    vehiculoMod.setMotor(String.valueOf(mVehiculo.cbMotor.getSelectedItem()));
                    vehiculoMod.setPotencia(Integer.parseInt(mVehiculo.txtPotencia.getText()));
                    vehiculoMod.setColor(mVehiculo.txtColor.getText());
                    vehiculoMod.setFechaFabricacion(Date.valueOf(mVehiculo.fechaFabModVeh.getDate()));
                    modelo.modificarVehiculo(vehiculoMod);
                    JOptionPane.showMessageDialog(null, "Vehiculo Modificado", "Vehiculo Modificado", JOptionPane.INFORMATION_MESSAGE);
                    list.listarVehiculos(modelo.getVehiculo(), administrador);
                }
                limpiarCampos.limpiarCamposModVeh(mVehiculo);
                break;

            case "EliminarVehiculoAdmin":
                if (administrador.listaVehiculosAdmin.getSelectedValue() != null) {

                    if (JOptionPane.showConfirmDialog(null, "¿Estas seguro que quieres eliminar este cliente?", "Eliminar Cliente", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                        Vehiculo vehiculoEliminar = (Vehiculo) administrador.listaVehiculosAdmin.getSelectedValue();
                        modelo.eliminarVehiculo(vehiculoEliminar);
                    } else {
                        JOptionPane.showMessageDialog(null, "El vehiculo no ha sido eliminado", "Vehiculo", JOptionPane.INFORMATION_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Vehiculo no seleccionada", "No seleccion", JOptionPane.WARNING_MESSAGE);
                }
                list.listarVehiculos(modelo.getVehiculo(), administrador);
                list.listarVentas(modelo.getVentas(), administrador);
                list.listarRevision(modelo.getRevision(), administrador);
                break;

            case "VisualizarDatosVeh":
                if (administrador.listaVehiculosAdmin.getSelectedValue() != null) {

                    administrador.setVisible(false);
                    administrador.dispose();
                    visVeh.setVisible(true);

                    Vehiculo vehiculoVisualizar = (Vehiculo) administrador.listaVehiculosAdmin.getSelectedValue();
                    visVeh.cbMarca.setSelectedItem(vehiculoVisualizar.getMarca());
                    visVeh.txtModelo.setText(vehiculoVisualizar.getModelo());
                    visVeh.cbTipoVeh.setSelectedItem(vehiculoVisualizar.getTipoVehiculo());
                    visVeh.txtDomicilio.setText(vehiculoVisualizar.getDomicilioVeh());
                    visVeh.cbMotor.setSelectedItem(vehiculoVisualizar.getMotor());
                    visVeh.txtPotencia.setText(String.valueOf(vehiculoVisualizar.getPotencia()));
                    visVeh.txtColor.setText(vehiculoVisualizar.getColor());
                    visVeh.fechaFabricacion.setDate(vehiculoVisualizar.getFechaFabricacion().toLocalDate());
                } else {
                    JOptionPane.showMessageDialog(null, "Vehiculo no seleccionada", "No seleccion", JOptionPane.WARNING_MESSAGE);
                }

                break;

            case "BuscarVehiculo":
                String vehBuscar = administrador.txtVehBuscar.getText();
                administrador.txtVehBuscar.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyTyped(KeyEvent keyEvent) {
                        if (administrador.txtVehBuscar.getText().length() == 0) {
                            list.listarVehiculos(modelo.getVehiculo(), administrador);
                        }
                    }
                });

                if (!vehBuscar.isEmpty()) {
                    list.listarVehiculos(modelo.getVehiculoNombre(vehBuscar), administrador);
                }
                break;

            case "InfomeVeh":
                InformesVeh informesVeh = new InformesVeh();
                informesVeh.informeVehiculoCompleto(modelo);
                break;

            case "InformeGraficoVeh":
                InformesVeh informesVehGraf = new InformesVeh();
                informesVehGraf.informeVehiculoGrafico(modelo);
                break;

            //VENTA
            case "NuevoVentAdm":
                if (camposVacios.camposVaciosVent(nVenta)) {
                    JOptionPane.showMessageDialog(null, "Existen campos vacios", "Campos Vacios", JOptionPane.WARNING_MESSAGE);
                } else if (!isNumeric(nVenta.txtCodVenta.getText()) || !isNumeric(nVenta.txtIVA.getText()) || !isNumeric(nVenta.txtPrecio.getText())) {
                    JOptionPane.showMessageDialog(null, "Debes introducir numeros", "Error", JOptionPane.ERROR_MESSAGE);
                } else if (modelo.existeVenta(Integer.parseInt(nVenta.txtCodVenta.getText()))) {
                    JOptionPane.showMessageDialog(null, "Existe el codigo de venta", "Existe Codigo Venta", JOptionPane.ERROR_MESSAGE);
                } else {
                    Ventas venta = new Ventas();
                    venta.setTipoConsulta(nVenta.txtConsulta.getText());
                    venta.setCodVenta(Integer.parseInt(nVenta.txtCodVenta.getText()));
                    venta.setIva(Integer.parseInt(nVenta.txtIVA.getText()));
                    venta.setFechaVenta(Date.valueOf(nVenta.fechaVenta.getDate()));
                    venta.setPrecioFinal(Double.parseDouble(nVenta.txtPrecio.getText()));
                    venta.setCliente((Cliente) nVenta.cbCliente.getSelectedItem());
                    venta.setObservaciones(nVenta.txtObservaciones.getText());
                    venta.setDescripcion(nVenta.txtDescripcion.getText());
                    modelo.altaVenta(venta);
                    JOptionPane.showMessageDialog(null, "Nueva venta añadida", "Venta añadida", JOptionPane.INFORMATION_MESSAGE);
                    list.listarVentas(modelo.getVentas(), administrador);
                    list.listarClientes(modelo.getCliente(), administrador);
                }
                limpiarCampos.limpiarCamposVen(nVenta);

                break;

            case "GuardarVentaAdm":
                if (camposVacios.camposVaciosModVent(mVenta)) {
                    JOptionPane.showMessageDialog(null, "Existen campos vacios", "Campos Vacios", JOptionPane.WARNING_MESSAGE);
                } else if (!isNumeric(mVenta.txtCodVenta.getText()) || !isNumeric(mVenta.txtIVA.getText()) || !isNumeric(mVenta.txtPrecio.getText())) {
                    JOptionPane.showMessageDialog(null, "Debes introducir numeros", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    Ventas ventaMod = (Ventas) administrador.listaVenta.getSelectedValue();
                    ventaMod.setTipoConsulta(mVenta.txtConsulta.getText());
                    ventaMod.setCodVenta(Integer.parseInt(mVenta.txtCodVenta.getText()));
                    ventaMod.setIva(Integer.parseInt(mVenta.txtIVA.getText()));
                    ventaMod.setFechaVenta(Date.valueOf(mVenta.fechaVenta.getDate()));
                    ventaMod.setPrecioFinal(Double.parseDouble(mVenta.txtPrecio.getText()));
                    ventaMod.setCliente((Cliente) mVenta.cbCliente.getSelectedItem());
                    ventaMod.setObservaciones(mVenta.txtObservaciones.getText());
                    ventaMod.setDescripcion(mVenta.txtDescripcion.getText());
                    modelo.modificarVenta(ventaMod);
                    JOptionPane.showMessageDialog(null, "Venta Modificada ", "Venta Modificada", JOptionPane.INFORMATION_MESSAGE);
                    list.listarVentas(modelo.getVentas(), administrador);
                }
                limpiarCampos.limpiarCamposModVen(mVenta);
                break;

            case "EliminarVentaAdm":
                if (administrador.listaVenta.getSelectedValue() != null) {

                    if (JOptionPane.showConfirmDialog(null, "¿Estas seguro que quieres eliminar este cliente?", "Eliminar Cliente", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                        Ventas venta = (Ventas) administrador.listaVenta.getSelectedValue();
                        modelo.eliminarVenta(venta);
                    } else {
                        JOptionPane.showMessageDialog(null, "La venta no ha sido eliminada", "Venta", JOptionPane.INFORMATION_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No has seleccionado una venta", "Ninguna selección", JOptionPane.WARNING_MESSAGE);
                }
                list.listarVentas(modelo.getVentas(), administrador);
                break;

            case "VisualizarDatosVenta":
                if (administrador.listaVenta.getSelectedValue() != null) {
                    visVent.setVisible(true);
                    administrador.setVisible(false);
                    administrador.dispose();

                    Ventas ventaVisualizar = (Ventas) administrador.listaVenta.getSelectedValue();
                    visVent.txtConsulta.setText(ventaVisualizar.getTipoConsulta());
                    visVent.txtCodVenta.setText(String.valueOf(ventaVisualizar.getCodVenta()));
                    visVent.txtIVA.setText(String.valueOf(ventaVisualizar.getIva()));
                    visVent.fechaVenta.setDate(ventaVisualizar.getFechaVenta().toLocalDate());
                    visVent.txtPrecio.setText(String.valueOf(ventaVisualizar.getPrecioFinal()));
                    visVent.txtObservaciones.setText(ventaVisualizar.getObservaciones());
                    visVent.txtDescripcion.setText(ventaVisualizar.getDescripcion());
                } else {
                    JOptionPane.showMessageDialog(null, "No has seleccionado una venta", "Ninguna selección", JOptionPane.WARNING_MESSAGE);
                }

                break;

            case "BuscarVenta":
                int buscarVenta = Integer.parseInt(administrador.txtBuscarVenta.getText());
                administrador.txtBuscarVenta.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyTyped(KeyEvent keyEvent) {
                        if (administrador.txtVehBuscar.getText().length() == 0) {
                            list.listarVentas(modelo.getVentas(), administrador);
                        }
                    }
                });

                if (!String.valueOf(buscarVenta).isEmpty()) {
                    list.listarVentas(modelo.getVentasCodVenta(buscarVenta), administrador);
                }
                break;

            case "FacturaVenta":
                if (administrador.listaVenta.getSelectedValue() != null) {
                    Ventas ventas = (Ventas) administrador.listaVenta.getSelectedValue();
                    InformesVent informesVent = new InformesVent();
                    informesVent.informeFactura(ventas.getId(), modelo, ventas);
                } else {
                    JOptionPane.showMessageDialog(null, "No has seleccionado una venta", "Ninguna selección", JOptionPane.WARNING_MESSAGE);
                }
                break;

            case "InformeTotalVenta":
                InformesVent informesVentTotal = new InformesVent();
                informesVentTotal.informVentCompleto(modelo);
                break;

            case "GraficoVenta":
                InformesVent informesVentGraf = new InformesVent();
                informesVentGraf.informVentGrafico(modelo);
                break;

            case "AñadirVentaVehiculo":
                if (administrador.listaVenta.getSelectedValue() == null && administrador.listaVentaVehiculo.getSelectedValue() == null) {
                    JOptionPane.showMessageDialog(null, "Algun campo te falta por seleccionar", "Selección campos", JOptionPane.WARNING_MESSAGE);
                } else {
                    Ventas ventas = (Ventas) administrador.listaVenta.getSelectedValue();
                    Vehiculo vehiculo = (Vehiculo) administrador.listaVentaVehiculo.getSelectedValue();
                    ventas.getVehiculos().add(vehiculo);
                    vehiculo.getVentas().add(ventas);
                    modelo.modificarVenta(ventas);
                    modelo.modificarVehiculo(vehiculo);
                    administrador.dlmVentaVehiculo.clear();
                }

                list.listarVehiculos(modelo.getVehiculo(), administrador);
                list.listarVentas(modelo.getVentas(), administrador);
                break;

            case "Listar":
                if (administrador.listaVehiculosAdmin.getSelectedValue() != null) {
                    list.listarVentaVehiculos(administrador);
                } else {
                    JOptionPane.showMessageDialog(null, "No has seleccionado ningun vehiculo", "Ninguna selección", JOptionPane.WARNING_MESSAGE);
                }
                break;
        }
        rellenarCombox();

    }

    /**
     * Metodo con el que rellenaremos las combobox con los datos de otras clases
     */
    private void rellenarCombox() {
        nRevVeh.cbVehiculosRevision.removeAllItems();
        modRevVeh.cbVehiculoRevisionMod.removeAllItems();
        mVenta.cbCliente.removeAllItems();
        nVenta.cbCliente.removeAllItems();

        modelo.setComboVehiculoRevision(modelo.getVehiculo(), nRevVeh.cbVehiculosRevision);
        modelo.setComboVehiculoRevisionMod(modelo.getVehiculo(), modRevVeh.cbVehiculoRevisionMod);
        modelo.setComboClienteVentaMod(modelo.getCliente(), mVenta.cbCliente);
        modelo.setComboClienteVenta(modelo.getCliente(), nVenta.cbCliente);
    }

    /**
     * Metodo el cual comprueba si el datos pasado como parámetro es un número o no
     *
     * @param cadena
     * @return
     */
    public boolean isNumeric(String cadena) {
        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            try {
                Float.parseFloat(cadena);
                resultado = true;
            } catch (NumberFormatException excepcion2) {
                resultado = false;
            }
        }
        return resultado;
    }

    /**
     * Metodo con el cual se nos mostrarán los datos que queramos para su posterior modificación
     *
     * @param v
     */
    @Override
    public void valueChanged(ListSelectionEvent v) {
        if (v.getValueIsAdjusting()) {
            if (v.getSource() == administrador.listaUsuarios) {
                Usuarios usuarioSeleccion = (Usuarios) administrador.listaUsuarios.getSelectedValue();
                modUser.txtNombUserMod.setText(usuarioSeleccion.getNombre());
                modUser.txtApellMod.setText(usuarioSeleccion.getApellidos());
                modUser.txtEmailMod.setText(usuarioSeleccion.getEmail());
                modUser.fechaNacModUser.setDate(usuarioSeleccion.getFechaNacimiento().toLocalDate());
                modUser.txtNickMod.setText(usuarioSeleccion.getNick());
                modUser.txtContrMod.setText(usuarioSeleccion.getPasswd());
                modUser.cbPermMod.setSelectedItem(usuarioSeleccion.getPermiso());
            }
            if (v.getSource() == administrador.listaClientes) {
                Cliente clienteSeleccion = (Cliente) administrador.listaClientes.getSelectedValue();
                try {
                    mClient.txtNombreClienMod.setText(clienteSeleccion.getNombre());
                    mClient.txtApeClienMod.setText(clienteSeleccion.getApellidos());
                    mClient.txtDniClienMod.setText(clienteSeleccion.getDni());
                    mClient.FechaNacClienMod.setDate(clienteSeleccion.getFechaNacimiento().toLocalDate());
                    mClient.txtEmailClienMod.setText(Cifrado.desencriptar(clienteSeleccion.getEmail(), "cheetah2"));
                    mClient.txtTelClienMod.setText(String.valueOf(clienteSeleccion.getTelefono()));
                    mClient.txtDireCienMod.setText(Cifrado.desencriptar(clienteSeleccion.getDireccion(), "cheetah"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                } catch (NoSuchPaddingException e) {
                    e.printStackTrace();
                } catch (IllegalBlockSizeException e) {
                    e.printStackTrace();
                } catch (BadPaddingException e) {
                    e.printStackTrace();
                }
                mClient.txtCPClienMod.setText(String.valueOf(clienteSeleccion.getCodPostal()));
            }
            if (v.getSource() == administrador.listaVehiculosAdmin) {
                Vehiculo vehiculoSeleccion = (Vehiculo) administrador.listaVehiculosAdmin.getSelectedValue();
                mVehiculo.cbModVeh.setSelectedItem(vehiculoSeleccion.getMarca());
                mVehiculo.txtModelo.setText(vehiculoSeleccion.getModelo());
                mVehiculo.cbTipoVeh.setSelectedItem(vehiculoSeleccion.getTipoVehiculo());
                mVehiculo.txtDomVeh.setText(vehiculoSeleccion.getDomicilioVeh());
                mVehiculo.cbMotor.setSelectedItem(vehiculoSeleccion.getMotor());
                mVehiculo.txtPotencia.setText(String.valueOf(vehiculoSeleccion.getPotencia()));
                mVehiculo.txtColor.setText(vehiculoSeleccion.getColor());
                mVehiculo.fechaFabModVeh.setDate(vehiculoSeleccion.getFechaFabricacion().toLocalDate());
            }
            if (v.getSource() == administrador.listaRevision) {
                Revision revisionSeleccion = (Revision) administrador.listaRevision.getSelectedValue();
                modRevVeh.fechaRadiador.setDate(revisionSeleccion.getCambioRadiador().toLocalDate());
                modRevVeh.fechaNeumatico.setDate(revisionSeleccion.getCambioNeumaticos().toLocalDate());
                modRevVeh.fechaFreno.setDate(revisionSeleccion.getCambioFrenos().toLocalDate());
                modRevVeh.fechaAceite.setDate(revisionSeleccion.getCambioAceite().toLocalDate());
                modRevVeh.fechaAmortiguador.setDate(revisionSeleccion.getCambioAmortiguador().toLocalDate());
                modRevVeh.fechaFiltro.setDate(revisionSeleccion.getCambioFiltro().toLocalDate());
                modRevVeh.fechaLuces.setDate(revisionSeleccion.getCambioLuces().toLocalDate());
                modRevVeh.cbVehiculoRevisionMod.setSelectedItem(revisionSeleccion.getVehiculo());
            }
            if (v.getSource() == administrador.listaVenta) {
                Ventas ventaSeleccion = (Ventas) administrador.listaVenta.getSelectedValue();
                mVenta.txtConsulta.setText(ventaSeleccion.getTipoConsulta());
                mVenta.txtCodVenta.setText(String.valueOf(ventaSeleccion.getCodVenta()));
                mVenta.txtIVA.setText(String.valueOf(ventaSeleccion.getIva()));
                mVenta.fechaVenta.setDate(ventaSeleccion.getFechaVenta().toLocalDate());
                mVenta.txtPrecio.setText(String.valueOf(ventaSeleccion.getPrecioFinal()));
                mVenta.cbCliente.setSelectedItem(ventaSeleccion.getCliente());
                mVenta.txtObservaciones.setText(ventaSeleccion.getObservaciones());
                mVenta.txtDescripcion.setText(ventaSeleccion.getDescripcion());
                list.listarVehiculosVenta(modelo.getVehiculo(), administrador);
            }
        }
    }
}