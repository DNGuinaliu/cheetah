package com.davidng.cheetah.gui.Informes;

import com.davidng.cheetah.gui.Modelo;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.view.JasperViewer;

import java.util.HashMap;

/**
 * @author dnadeau
 * Clase donde tendremos los diferentes metodos para crear informes de clientes
 */
public class InformesClient {

    /**
     * Metodo con el que crearemos un informe con todos los registros de los clientes
     *
     * @param modelo Objeto de la clase Modelo
     */
    public void informClientCompleto(Modelo modelo) {

        try {
            JasperReport reporte = JasperCompileManager.compileReport("src\\Informes\\Cliente.jrxml");
            JasperPrint print = JasperFillManager.fillReport(reporte, null, modelo.conexion);
            JasperViewer viewer = new JasperViewer(print, false);
            viewer.show();


            JasperExportManager.exportReportToPdfFile(print, "Informes\\Totales\\Clientes.pdf");
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    /**
     * Metodo con el que crearemos un informe con los datos del cliente buscado
     * @param modelo Objeto de la clase Modelo
     * @param dni Variable con la que buscaremos al cliente
     */
    public void informClientBusqueda(Modelo modelo, String dni) {
        try {
            HashMap<String, Object> parametros = new HashMap<>();
            parametros.put("dniBuscar", dni);
            JasperReport reporte = JasperCompileManager.compileReport("src\\Informes\\buscarCliente.jrxml");
            JasperPrint print = JasperFillManager.fillReport(reporte, parametros, modelo.conexion);
            JasperViewer viewer = new JasperViewer(print, false);
            viewer.show();


            JasperExportManager.exportReportToPdfFile(print, "Informes\\Busqueda\\ClientesBuscado.pdf");
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }
}
