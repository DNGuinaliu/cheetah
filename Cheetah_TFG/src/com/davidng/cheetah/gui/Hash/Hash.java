package com.davidng.cheetah.gui.Hash;

/**
 * @author dnadeau
 * Clase donde tendremos el metodo para poder hashear la contraseña de los usuarios
 */
public class Hash {
    /**
     * Metodo donde recogeremos el dato que queramos hashear y el tipo de hasheo y se procedera a hashearlo
     *
     * @param txt
     * @param hashType
     * @return nos devuelve el dato pasado como parámetro hasheado
     */
    public static String getHash(String txt, String hashType) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance(hashType);
            byte[] array = md.digest(txt.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    /**
     * Metodo donde le pasamos el tipo de dato a hashear
     *
     * @param txt
     * @return nos devuelve el dato pasado como parámetro hasheado
     */
    public static String md5(String txt) {
        return Hash.getHash(txt, "MD5");
    }

    /**
     * Metodo donde le pasamos el tipo de dato a hashear
     *
     * @param txt
     * @return nos devuelve el dato pasado como parámetro hasheado
     */
    public static String sha1(String txt) {
        return Hash.getHash(txt, "SHA1");
    }
}
