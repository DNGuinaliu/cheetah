package com.davidng.cheetah.gui.Usuarios;

import javax.swing.*;
import java.awt.*;

/**
 * @author dnadeau
 * Clase donde tendremos la ventana de login
 */
public class VistaUsuarios extends JFrame  {

    private JPanel panel1;
    public JTextField txtNombreUsuario;
    public JPasswordField txtContraseña;
    public JButton validarButton;


    /**
     * Constructor de la clase donde se inicializara la ventana
     */
    public VistaUsuarios(){
        this.setTitle("CHEETAH");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth() + 200, this.getHeight() + 100));
        this.setLocationRelativeTo(null);
        this.setIconImage(getIconImage());

    }

    /**
     * Metodo que nos mostrara un icono en la parte superior izquierda de la ventana
     * @return retValue
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("cheetah.png"));
        return retValue;
    }
}
