package com.davidng.cheetah.gui.Usuarios;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

/**
 * @author dnadeau
 * Clase donde tendremos la ventana para crear un usuario nuevo en caso de que
 * no exista ninguno
 */
public class NuevoUsuario extends JFrame {
    private JPanel panel2;
    public JTextField txtNombreUsuario;
    public JPasswordField txtNuevaContraseña;
    public JComboBox cbPermisoUser;
    public JButton nuevoUserButton;
    public JButton volverButton;
    public JTextField txtApellidoUsuario;
    public JTextField txtEmailUsuario;
    public JTextField txtNickUsuario;
    public DatePicker fechaNacUser;

    /**
     * Constructor de la clase donde inicializaremos los datos
     */
    public NuevoUsuario() {
        this.setTitle("CHEETAH");
        initDialog();
        setComboBoxDatosUser();
    }

    /**
     * Metodo para inicializar la ventana
     */
    private void initDialog() {
        this.setContentPane(panel2);
        this.panel2.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth() + 500, this.getHeight() + 100));
        this.setVisible(false);
        this.setLocationRelativeTo(null);
        this.setIconImage(getIconImage());
    }

    /**
     * Metodo con el que añadiremos un dato a la combobox
     */
    private void setComboBoxDatosUser() {

        cbPermisoUser.addItem("Administrador");

        cbPermisoUser.setSelectedItem(-1);
    }

    /**
     * Metodo que nos mostrara un icono en la parte superior izquierda de la ventana
     * @return retValue
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("cheetah.png"));


        return retValue;
    }


}
