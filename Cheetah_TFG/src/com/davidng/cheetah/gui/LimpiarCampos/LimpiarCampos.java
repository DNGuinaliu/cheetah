package com.davidng.cheetah.gui.LimpiarCampos;

import com.davidng.cheetah.gui.Usuarios.NuevoUsuario;
import com.davidng.cheetah.gui.Usuarios.VistaUsuarios;
import com.davidng.cheetah.gui.Ventanas.Clientes.ModificarCliente;
import com.davidng.cheetah.gui.Ventanas.Clientes.NuevoCliente;
import com.davidng.cheetah.gui.Ventanas.Revision.ModRevVeh;
import com.davidng.cheetah.gui.Ventanas.Revision.NuevRevVeh;
import com.davidng.cheetah.gui.Ventanas.Usuarios.ModificarUsuario;
import com.davidng.cheetah.gui.Ventanas.Usuarios.NuevoUserAdmin;
import com.davidng.cheetah.gui.Ventanas.Vehiculos.ModificarVehiculo;
import com.davidng.cheetah.gui.Ventanas.Vehiculos.NuevoVehiculo;
import com.davidng.cheetah.gui.Ventanas.Ventas.ModificarVenta;
import com.davidng.cheetah.gui.Ventanas.Ventas.NuevaVenta;

/**
 * @author dnadeau
 * Clase donde tendremos los diferentes metodos para limpiar los campos de las ventanas
 */
public class LimpiarCampos {


    //USUARIO

    /**
     * Metodo con el cual limpiaremos los campos del login
     *
     * @param usuario Objeto de la clase VistaUsuarios
     */
    public void limpiarCamposLogin(VistaUsuarios usuario) {
        usuario.txtNombreUsuario.setText("");
        usuario.txtContraseña.setText("");
    }

    /**
     * Metodo con el cual limpiaremos los campos del crear un usuario si no existe ninguno
     *
     * @param nuevoUsuario Objeto de la clase NuevoUsuario
     */
    public void limpiarCamposRegistro(NuevoUsuario nuevoUsuario) {
        nuevoUsuario.txtNombreUsuario.setText("");
        nuevoUsuario.txtApellidoUsuario.setText("");
        nuevoUsuario.txtEmailUsuario.setText("");
        nuevoUsuario.fechaNacUser.setText("");
        nuevoUsuario.txtNickUsuario.setText("");
        nuevoUsuario.txtNuevaContraseña.setText("");
        nuevoUsuario.cbPermisoUser.setSelectedIndex(-1);
    }

    /**
     * Metodo con el cual limpiaremos los campos del crear un usuario desde administrador
     *
     * @param nUserAdm Objeto de la clase NuevoUserAdmin
     */
    public void limpiarCamposNuevoUser(NuevoUserAdmin nUserAdm) {
        nUserAdm.txtNomUserNuev.setText("");
        nUserAdm.txtApeUserNuev.setText("");
        nUserAdm.txtEmaiUserNuev.setText("");
        nUserAdm.fechaNacNuevUser.setText("");
        nUserAdm.txtNickUserNuev.setText("");
        nUserAdm.txtPasswdUserNuev.setText("");
        nUserAdm.cbPermUserNuev.setSelectedIndex(-1);
    }

    /**
     * Metodo con el cual limpiaremos los campos del modificar un usuario desde administrador
     *
     * @param modUser Objeto de la clase ModificarUsuario
     */
    public void limpiarCamposModificarUser(ModificarUsuario modUser) {
        modUser.txtNombUserMod.setText("");
        modUser.txtApellMod.setText("");
        modUser.txtEmailMod.setText("");
        modUser.fechaNacModUser.setText("");
        modUser.txtNickMod.setText("");
        modUser.txtContrMod.setText("");
        modUser.cbPermMod.setSelectedIndex(-1);
    }

    //CLIENTE

    /**
     * Metodo con el cual limpiaremos los campos del crear un cliente
     *
     * @param nClient Objeto de la clase NuevoCliente
     */
    public void limpiarCamposNuevClient(NuevoCliente nClient) {
        nClient.txtNomClien.setText("");
        nClient.txtApeClien.setText("");
        nClient.txtDniClient.setText("");
        nClient.fechaNacClien.setText("");
        nClient.txtEmailClien.setText("");
        nClient.txtTelefClien.setText("");
        nClient.txtDireccClien.setText("");
        nClient.txtCPClien.setText("");
    }

    /**
     * Metodo con el cual limpiaremos los campos del modificar un cliente
     *
     * @param mClient Objeto de la clase NuevoCliente
     */
    public void limpiarCamposModClient(ModificarCliente mClient) {
        mClient.txtNombreClienMod.setText("");
        mClient.txtApeClienMod.setText("");
        mClient.txtDniClienMod.setText("");
        mClient.FechaNacClienMod.setText("");
        mClient.txtEmailClienMod.setText("");
        mClient.txtTelClienMod.setText("");
        mClient.txtDireCienMod.setText("");
        mClient.txtCPClienMod.setText("");
    }

    //REVISION

    /**
     * Metodo con el cual limpiaremos los campos del crear una revisión
     *
     * @param nRevVeh Objeto de la clase NuevRevVeh
     */
    public void limpiarCamposRev(NuevRevVeh nRevVeh) {
        nRevVeh.fechaRadiador.setText("");
        nRevVeh.fechaNeumaticos.setText("");
        nRevVeh.fechaLuces.setText("");
        nRevVeh.fechaFrenos.setText("");
        nRevVeh.fechaFiltro.setText("");
        nRevVeh.fechaAmortiguador.setText("");
        nRevVeh.fechaAceite.setText("");
        nRevVeh.cbVehiculosRevision.setSelectedIndex(-1);
    }

    /**
     * Metodo con el cual limpiaremos los campos del modificar una revisión
     *
     * @param modRevVeh Objeto de la clase ModRevVeh
     */
    public void limpiarCamposModRev(ModRevVeh modRevVeh) {
        modRevVeh.fechaRadiador.setText("");
        modRevVeh.fechaNeumatico.setText("");
        modRevVeh.fechaLuces.setText("");
        modRevVeh.fechaFreno.setText("");
        modRevVeh.fechaFiltro.setText("");
        modRevVeh.fechaAmortiguador.setText("");
        modRevVeh.fechaAceite.setText("");
        modRevVeh.cbVehiculoRevisionMod.setSelectedIndex(-1);
    }

    //VEHICULO

    /**
     * Metodo con el cual limpiaremos los campos del crear un vehiculo
     *
     * @param nVehiculo Objeto de la clase NuevoVehiculo
     */
    public void limpiarCamposVeh(NuevoVehiculo nVehiculo) {
        nVehiculo.cbMarcaVeh.setSelectedIndex(-1);
        nVehiculo.txtModeloVeh.setText("");
        nVehiculo.txtDomicilio.setText("");
        nVehiculo.cbTipoVeh.setSelectedItem(0);
        nVehiculo.txtColor.setText("");
        nVehiculo.txtPotencia.setText("");
        nVehiculo.fechaFabriVeh.setText("");
        nVehiculo.cbMotor.setSelectedIndex(-1);
    }

    /**
     * Metodo con el cual limpiaremos los campos del modificar un vehiculo
     *
     * @param mVehiculo Objeto de la clase ModificarVehiculo
     */
    public void limpiarCamposModVeh(ModificarVehiculo mVehiculo) {
        mVehiculo.cbModVeh.setSelectedIndex(-1);
        mVehiculo.txtModelo.setText("");
        mVehiculo.txtDomVeh.setText("");
        mVehiculo.cbTipoVeh.setSelectedItem(0);
        mVehiculo.txtColor.setText("");
        mVehiculo.txtPotencia.setText("");
        mVehiculo.fechaFabModVeh.setText("");
        mVehiculo.cbMotor.setSelectedIndex(-1);
    }

    //VENTA

    /**
     * Metodo con el cual limpiaremos los campos del crear una venta
     *
     * @param nVenta Objeto de la clase NuevaVenta
     */
    public void limpiarCamposVen(NuevaVenta nVenta) {
        nVenta.txtConsulta.setText("");
        nVenta.txtCodVenta.setText("");
        nVenta.txtIVA.setText("");
        nVenta.txtPrecio.setText("");
        nVenta.txtDescripcion.setText("");
        nVenta.txtObservaciones.setText("");
        nVenta.fechaVenta.setText("");
        nVenta.cbCliente.setSelectedIndex(-1);
    }

    /**
     * Metodo con el cual limpiaremos los campos del modificar una venta
     *
     * @param mVenta Objeto de la clase ModificarVenta
     */
    public void limpiarCamposModVen(ModificarVenta mVenta) {
        mVenta.txtConsulta.setText("");
        mVenta.txtCodVenta.setText("");
        mVenta.txtIVA.setText("");
        mVenta.txtPrecio.setText("");
        mVenta.txtDescripcion.setText("");
        mVenta.txtObservaciones.setText("");
        mVenta.fechaVenta.setText("");
        mVenta.cbCliente.setSelectedIndex(-1);
    }


}
