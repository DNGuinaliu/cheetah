package com.davidng.cheetah.Enum;

/**
 * @author dnadeau
 * Clase enumerada donde tendremos creadas varias marcas de vehiculos
 */
public enum Marcas {
    BMW("BMW"),
    DACIA("Dacia"),
    KIA("Kia"),
    LANDROVER("Land Rover"),
    MERCEDESBENZ("Mercedes-Benz"),
    OPEL("Opel"),
    SUBARU("Subaru"),
    VOLKSWAGEN("Volkswagen"),
    MINI("Mini"),
    PEUGEOT("Peugeot"),
    SEAT("Seat"),
    VOLVO("Volvo"),
    FIAT("Fiat"),
    MITSUBISHI("Mitsubishi"),
    SKODA("Skoda"),
    AUDI("Audi"),
    CHEVROLET ("Chevrolet"),
    FORD("Ford"),
    SMART("Smart"),
    CITROEN("Citroën"),
    JEEP("Jeep"),
    MAZDA("Mazda"),
    NISSAN("Nissan"),
    RENAULT("Renault"),
    TOYOTA("Toyota");


    private String marca; //Variable donde se recogerán los datos enumerados anteriores

    /**
     * Constructor de la clase Marca donde se inicializa la variable String
     * @param marca Variable donde se recogerán los datos enumerados anteriores
     */
    Marcas(String marca) {
        this.marca = marca;
    }

    /**
     * Metodo que nos devolvera la marca
     * @return marca
     */
    public String getMarca() {
        return marca;
    }
}
