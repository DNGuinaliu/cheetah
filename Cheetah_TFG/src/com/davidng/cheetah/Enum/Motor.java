package com.davidng.cheetah.Enum;

/**
 * @author dnadeau
 * Clase enumerada donde tendremos creadas varias tipo de motor de vehiculos
 */
public enum Motor {
    GASOLINA("Gasolina"),
    DIESEL("Diesel"),
    ELECTRICOS("Electricos"),
    GLPyGNC("GLP y GNC"),
    HIBRIDOS("Híbridos"),
    ROTATIVO("Rotativo");


    private String motor; //Variable donde se recogerán los datos enumerados anteriores

    /**
     * Constructor de la clase Motor donde se inicializa la variable String
     * @param motor Variable donde se recogerán los datos enumerados
     */
    Motor(String motor) {
        this.motor = motor;
    }

    /**
     * Metodo que nos devolvera el tipo de motor
     * @return motor
     */
    public String getMotor() {
        return motor;
    }
}
