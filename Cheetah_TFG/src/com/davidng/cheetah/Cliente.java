package com.davidng.cheetah;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Cliente {
    private int idcliente;
    private String nombre;
    private String apellidos;
    private String dni;
    private int telefono;
    private Date fechaNacimiento;
    private String direccion;
    private String email;
    private int codPostal;
    private List<Ventas> ventas;

    /**
     * Constructor vacio de la clase Cliente
     */
    public Cliente() {
    }

    /**
     * Constructor de la clase Cliente donde se inicializaran los datos menos la id
     *
     * @param nombre
     * @param apellidos
     * @param dni
     * @param telefono
     * @param fechaNacimiento
     * @param direccion
     * @param email
     * @param codPostal
     */
    public Cliente(String nombre, String apellidos, String dni, int telefono, Date fechaNacimiento, String direccion, String email, int codPostal) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.dni = dni;
        this.telefono = telefono;
        this.fechaNacimiento = fechaNacimiento;
        this.direccion = direccion;
        this.email = email;
        this.codPostal = codPostal;
    }

    /**
     * Constructor de la clase Cliente donde se inicializaran los datos
     *
     * @param idcliente
     * @param nombre
     * @param apellidos
     * @param dni
     * @param telefono
     * @param fechaNacimiento
     * @param direccion
     * @param email
     * @param codPostal
     */
    public Cliente(int idcliente, String nombre, String apellidos, String dni, int telefono, Date fechaNacimiento, String direccion, String email, int codPostal) {
        this.idcliente = idcliente;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.dni = dni;
        this.telefono = telefono;
        this.fechaNacimiento = fechaNacimiento;
        this.direccion = direccion;
        this.email = email;
        this.codPostal = codPostal;
    }

    @Id
    @Column(name = "idcliente")
    public int getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(int idcliente) {
        this.idcliente = idcliente;
    }

    @Basic
    @Column(name = "nombre")
    /**
     * Metodo con el que devolvemos el nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo con el que dar valor al nombre
     *
     * @param nombre Variable con la que introduciremos el nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    /**
     *  Metodo con el que devolvemos los apellidos
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * Metodo con el que dar valor a los apellidos
     *
     * @param apellidos Variable con la que introduciremos los apellidos
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "dni")
    /**
     * Metodo con el que devolvemos el dni
     */
    public String getDni() {
        return dni;
    }

    /**
     * Metodo con el que dar valor al dni
     *
     * @param dni Variable con la que introduciremos el dni
     */
    public void setDni(String dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "telefono")
    /**
     * Metodo con el que devolvemos el telefono
     */
    public int getTelefono() {
        return telefono;
    }

    /**
     * Metodo con el que dar valor al telefono
     *
     * @param telefono Variable con la que introduciremos el telefono
     */
    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    @Basic
    @Column(name = "fecha_nacimiento")
    /**
     * Metodo con el que devolvemos la fecha de nacimiento
     */
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * Metodo con el que dar valor a la fecha de nacimiento
     *
     * @param fechaNacimiento Variable con la que introduciremos la fecha de nacimiento
     */
    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Basic
    @Column(name = "direccion")
    /**
     * Metodo con el que devolvemos la dirección
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Metodo con el que dar valor a la dirección
     *
     * @param direccion Variable con la que introduciremos la dirección
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "email")
    /**
     * Metodo con el que devolvemos el email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Metodo con el que dar valor al email
     *
     * @param email Variable con la que introduciremos el email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "cod_postal")
    /**
     * Metodo con el que devolvemos el codigo postal
     */
    public int getCodPostal() {
        return codPostal;
    }

    /**
     * Metodo con el que dar valor al codigo postal
     * @param codPostal Variable con la que introduciremos el codigo postal
     */
    public void setCodPostal(int codPostal) {
        this.codPostal = codPostal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return idcliente == cliente.idcliente &&
                telefono == cliente.telefono &&
                codPostal == cliente.codPostal &&
                Objects.equals(nombre, cliente.nombre) &&
                Objects.equals(apellidos, cliente.apellidos) &&
                Objects.equals(dni, cliente.dni) &&
                Objects.equals(fechaNacimiento, cliente.fechaNacimiento) &&
                Objects.equals(direccion, cliente.direccion) &&
                Objects.equals(email, cliente.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idcliente, nombre, apellidos, dni, telefono, fechaNacimiento, direccion, email, codPostal);
    }

    @OneToMany(mappedBy = "cliente")
    public List<Ventas> getVentas() {
        return ventas;
    }

    public void setVentas(List<Ventas> ventas) {
        this.ventas = ventas;
    }

    @Override
    public String toString() {
        return nombre +
                ", " + apellidos +
                ", " + dni;
    }
}
