package com.davidng.cheetah;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Vehiculo {
    private int idvehiculo;
    private String modelo;
    private String marca;
    private String domicilioVeh;
    private String tipoVehiculo;
    private int potencia;
    private String motor;
    private Date fechaFabricacion;
    private String color;
    private List<Revision> revisiones;
    private List<Ventas> ventas;

    /**
     * Constructor vacio de la clase Vehiculo
     */
    public Vehiculo() {
    }

    /**
     * Constructor de la clase Vehiculo donde se inicializaran los datos menos la id
     *
     * @param modelo
     * @param marca
     * @param domicilioVeh
     * @param tipoVehiculo
     * @param potencia
     * @param motor
     * @param fechaFabricacion
     * @param color
     */
    public Vehiculo(String modelo, String marca, String domicilioVeh, String tipoVehiculo, int potencia, String motor, Date fechaFabricacion, String color) {
        this.modelo = modelo;
        this.marca = marca;
        this.domicilioVeh = domicilioVeh;
        this.tipoVehiculo = tipoVehiculo;
        this.potencia = potencia;
        this.motor = motor;
        this.fechaFabricacion = fechaFabricacion;
        this.color = color;
    }

    /**
     * Constructor de la clase Vehiculo donde se inicializaran los datos
     *
     * @param idvehiculo
     * @param modelo
     * @param marca
     * @param domicilioVeh
     * @param tipoVehiculo
     * @param potencia
     * @param motor
     * @param fechaFabricacion
     * @param color
     */
    public Vehiculo(int idvehiculo, String modelo, String marca, String domicilioVeh, String tipoVehiculo, int potencia, String motor, Date fechaFabricacion, String color) {
        this.idvehiculo = idvehiculo;
        this.modelo = modelo;
        this.marca = marca;
        this.domicilioVeh = domicilioVeh;
        this.tipoVehiculo = tipoVehiculo;
        this.potencia = potencia;
        this.motor = motor;
        this.fechaFabricacion = fechaFabricacion;
        this.color = color;
    }

    @Id
    @Column(name = "idvehiculo")
    public int getIdvehiculo() {
        return idvehiculo;
    }

    public void setIdvehiculo(int idvehiculo) {
        this.idvehiculo = idvehiculo;
    }

    @Basic
    @Column(name = "modelo")
    /**
     * Metodo con el que devolvemos el modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * Metodo con el que dar valor al modelo
     *
     * @param modelo Variable con la que introduciremos el modelo
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    @Basic
    @Column(name = "marca")
    /**
     * Metodo con el que devolvemos la marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Metodo con el que dar valor a la marca
     *
     * @param marca Variable con la que introduciremos la marca
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Basic
    @Column(name = "domicilio_veh")
    /**
     * Metodo con el que devolvemos el domicilio
     */
    public String getDomicilioVeh() {
        return domicilioVeh;
    }

    /**
     * Metodo con el que dar valor al domicilio
     *
     * @param domicilioVeh Variable con la que introduciremos el domicilio
     */
    public void setDomicilioVeh(String domicilioVeh) {
        this.domicilioVeh = domicilioVeh;
    }

    @Basic
    @Column(name = "tipo_vehiculo")
    /**
     * Metodo con el que devolvemos el tipo de vehiculo
     */
    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    /**
     * Metodo con el que dar valor al tipo de vehiculo
     *
     * @param tipoVehiculo Variable con la que introduciremos el tipo de vehiculo
     */
    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    @Basic
    @Column(name = "potencia")
    /**
     * Metodo con el que devolvemos la potencia
     */
    public int getPotencia() {
        return potencia;
    }

    /**
     * Metodo con el que dar valor a la potencia
     *
     * @param potencia Variable con la que introduciremos la potencia
     */
    public void setPotencia(int potencia) {
        this.potencia = potencia;
    }

    @Basic
    @Column(name = "motor")
    /**
     *  Metodo con el que devolvemos el motor
     */
    public String getMotor() {
        return motor;
    }

    /**
     * Metodo con el que dar valor al motor
     *
     * @param motor Variable con la que introduciremos el motor
     */
    public void setMotor(String motor) {
        this.motor = motor;
    }

    @Basic
    @Column(name = "fecha_fabricacion")
    /**
     * Metodo con el que devolvemos la fecha de fabricación
     */
    public Date getFechaFabricacion() {
        return fechaFabricacion;
    }

    /**
     * Metodo con el que dar valor a la fecha de fabricación
     *
     * @param fechaFabricacion Variable con la que introduciremos la fecha de fabricación
     */
    public void setFechaFabricacion(Date fechaFabricacion) {
        this.fechaFabricacion = fechaFabricacion;
    }

    @Basic
    @Column(name = "color")
    /**
     * Metodo con el que devolvemos el color
     */
    public String getColor() {
        return color;
    }

    /**
     * Metodo con el que dar valor al color
     * @param color Variable con la que introduciremos al color
     */
    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehiculo vehiculo = (Vehiculo) o;
        return idvehiculo == vehiculo.idvehiculo &&
                potencia == vehiculo.potencia &&
                Objects.equals(modelo, vehiculo.modelo) &&
                Objects.equals(marca, vehiculo.marca) &&
                Objects.equals(domicilioVeh, vehiculo.domicilioVeh) &&
                Objects.equals(tipoVehiculo, vehiculo.tipoVehiculo) &&
                Objects.equals(motor, vehiculo.motor) &&
                Objects.equals(fechaFabricacion, vehiculo.fechaFabricacion) &&
                Objects.equals(color, vehiculo.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idvehiculo, modelo, marca, domicilioVeh, tipoVehiculo, potencia, motor, fechaFabricacion, color);
    }

    @OneToMany(mappedBy = "vehiculo")
    public List<Revision> getRevisiones() {
        return revisiones;
    }

    public void setRevisiones(List<Revision> revisiones) {
        this.revisiones = revisiones;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "vehiculo_ventas", catalog = "", schema = "cheetah", joinColumns = @JoinColumn(name = "id_vehiculo", referencedColumnName = "idvehiculo", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_ventas", referencedColumnName = "id", nullable = false))
    public List<Ventas> getVentas() {
        return ventas;
    }

    public void setVentas(List<Ventas> ventas) {
        this.ventas = ventas;
    }

    @Override
    public String toString() {
        return marca +
                ", " + modelo;
    }
}
