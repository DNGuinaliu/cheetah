package test;

import com.davidng.cheetah.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.junit.jupiter.api.*;

import java.sql.Date;

/**
 * @author dnadeau
 * Clase donde realizaremos las diferentes pruebas unitarias
 */
class Test {
    private static SessionFactory sessionFactory;
    private Session session;

    /**
     * Metodo con el cual conectaremos con las entidades y la base de datos
     */
    public void conectar() {

        Configuration configuracion = new Configuration();
        configuracion.configure("hibernate.cfg.xml");

        configuracion.addAnnotatedClass(Revision.class);
        configuracion.addAnnotatedClass(Vehiculo.class);
        configuracion.addAnnotatedClass(Cliente.class);
        configuracion.addAnnotatedClass(Ventas.class);
        configuracion.addAnnotatedClass(Usuarios.class);

        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();
        sessionFactory = configuracion.buildSessionFactory(ssr);
    }

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
        sessionFactory = Test.getSessionFactory();

        System.out.println("SessionFactory created");
    }

    @AfterAll
    static void tearDownAfterClass() throws Exception {
        sessionFactory.close();
        System.out.println("SessionFactory closed");
    }

    @BeforeEach
    void setUp() throws Exception {
        conectar();
        session = sessionFactory.openSession();
        System.out.println("Session created");
    }

    @AfterEach
    void tearDown() throws Exception {
        session.close();
        System.out.println("Session closed");
    }

    //CLIENTE
    @org.junit.jupiter.api.Test
    void testAltaCliente() {
        System.out.println("Running testCreate....");
        session.beginTransaction();
        Date miFecha = new Date(115, 6, 2);
        Cliente cliente = new Cliente("Perico", "Delgado", "75126694F", 205456851, miFecha, "Coso 201 25", "PericoDel@gmail.com", 203512);
        Integer id = (Integer) session.save(cliente);
        session.getTransaction().commit();
        Assertions.assertFalse(id > 0);
    }

    @org.junit.jupiter.api.Test
    void getCliente() {

        System.out.println("Running testGet....");
        Integer id = 1;

        Cliente cliente = session.find(Cliente.class, id);
        Assertions.assertEquals("mario", cliente.getNombre());
    }

    @org.junit.jupiter.api.Test
    void getClienteNotExists() {
        System.out.println("Running testGetNotExists....");
        Integer id = 55;
        Cliente cliente = session.find(Cliente.class, id);
        Assertions.assertNull(cliente);
    }

    @org.junit.jupiter.api.Test
    void testUpdateCliente() {
        Integer id = 1;
        Date miFecha = new Date(115, 6, 2);
        Cliente cliente = new Cliente(id, "PEPE", "Delgado", "75126694F", 123456789, miFecha, "Calle Valencia 1 5", "PericoDel@gmail.com", 203512);
        session.beginTransaction();
        session.update(cliente);
        session.getTransaction().commit();

        Cliente clienteUpd = session.find(Cliente.class, id);

        Assertions.assertEquals("PEPE", clienteUpd.getNombre());
    }


    @org.junit.jupiter.api.Test
    void testDeleteCliente() {
        System.out.println("Running testDelete...");
        Integer id = 1;
        Cliente cliente = session.find(Cliente.class, id);
        session.beginTransaction();
        session.delete(cliente);
        session.getTransaction().commit();

        Cliente clienteDel = session.find(Cliente.class, id);

        Assertions.assertNull(clienteDel);
    }
    //VEHICULO

    @org.junit.jupiter.api.Test
    void testAltaVehiculo() {
        System.out.println("Running testCreate....");
        session.beginTransaction();
        Date miFecha = new Date(115, 6, 2);
        Vehiculo vehiculo = new Vehiculo("Focus", "Ford", "Madrid", "Deportivo", 320, "Gasolina", miFecha, "Azul");
        Integer id = (Integer) session.save(vehiculo);
        session.getTransaction().commit();
        Assertions.assertFalse(id > 0);

    }

    @org.junit.jupiter.api.Test
    void getVehiculo() {

        System.out.println("Running testGet....");
        Integer id = 4;

        Vehiculo vehiculo = session.find(Vehiculo.class, id);
        Assertions.assertEquals("Ford", vehiculo.getMarca());
    }


    @org.junit.jupiter.api.Test
    void getVehiculoNotExists() {
        System.out.println("Running testGetNotExists....");
        Integer id = 55;
        Vehiculo vehiculo = session.find(Vehiculo.class, id);
        Assertions.assertNull(vehiculo);
    }

    @org.junit.jupiter.api.Test
    void testUpdateVehiculo() {
        Integer id = 4;
        Date miFecha = new Date(115, 6, 2);
        Vehiculo vehiculo = new Vehiculo(id, "Focus", "Ford", "Madrid", "Deportivo", 320, "Gasolina", miFecha, "Azul");
        session.beginTransaction();
        session.update(vehiculo);
        session.getTransaction().commit();

        Vehiculo vehiculoUpd = session.find(Vehiculo.class, id);

        Assertions.assertEquals("Ford", vehiculoUpd.getMarca());
    }

    @org.junit.jupiter.api.Test
    void testDeleteVehiculo() {
        System.out.println("Running testDelete...");
        Integer id = 4;
        Vehiculo vehiculo = session.find(Vehiculo.class, id);
        session.beginTransaction();
        session.delete(vehiculo);
        session.getTransaction().commit();

        Vehiculo vehiculoDel = session.find(Vehiculo.class, id);

        Assertions.assertNull(vehiculoDel);
    }

    //REVISION
    @org.junit.jupiter.api.Test
    void testAltaRevision() {
        System.out.println("Running testCreate....");
        session.beginTransaction();
        Date miFecha = new Date(115, 6, 2);
        Vehiculo vehiculo = new Vehiculo(6, "Focus", "Ford", "Madrid", "Deportivo", 320, "Gasolina", miFecha, "Azul");
        Revision revision = new Revision(miFecha, miFecha, miFecha, miFecha, miFecha, miFecha, miFecha, vehiculo);
        Integer id = (Integer) session.save(revision);
        session.getTransaction().commit();
        Assertions.assertFalse(id > 0);
    }

    @org.junit.jupiter.api.Test
    void getRevision() {

        System.out.println("Running testGet....");
        Integer id = 5;
        Date miFecha = new Date(115, 6, 2);
        Vehiculo vehiculo = new Vehiculo(5, "Focus", "Ford", "Madrid", "Deportivo", 320, "Gasolina", miFecha, "Azul");

        Revision revision = session.find(Revision.class, id);
        Assertions.assertEquals(vehiculo, revision.getVehiculo());
    }

    @org.junit.jupiter.api.Test
    void getRevisionNotExists() {
        System.out.println("Running testGetNotExists....");
        Integer id = 55;
        Revision revision = session.find(Revision.class, id);
        Assertions.assertNull(revision);
    }

    @org.junit.jupiter.api.Test
    void testUpdateRevision() {
        Integer id = 7;
        Date miFecha = new Date(115, 6, 2);
        Date mifecha2 = new Date(110,7,1);
        Vehiculo vehiculo = new Vehiculo(6,"Focus", "Ford", "Madrid", "Deportivo", 320, "Gasolina", miFecha, "Azul");
        Revision revision = new Revision(id,mifecha2, miFecha, mifecha2, miFecha, miFecha, miFecha, miFecha, vehiculo);

        session.beginTransaction();
        session.saveOrUpdate(revision);
        session.getTransaction().commit();

        Revision revisionUpd = session.find(Revision.class, id);

        Assertions.assertEquals(vehiculo, revisionUpd.getVehiculo());
    }

    @org.junit.jupiter.api.Test
    void testDeleteRevision() {
        System.out.println("Running testDelete...");
        Integer id = 5;
        Revision revision = session.find(Revision.class, id);
        session.beginTransaction();
        session.delete(revision);
        session.getTransaction().commit();

        Revision revisionoDel = session.find(Revision.class, id);

        Assertions.assertNull(revisionoDel);
    }

    //VENTAS
    @org.junit.jupiter.api.Test
    void testAltaVenta() {
        System.out.println("Running testCreate....");
        session.beginTransaction();
        Date miFecha = new Date(115, 6, 2);
        Cliente cliente = new Cliente(8, "Perico", "Delgado", "75126694F", 205456851, miFecha, "Coso 201 25", "PericoDel@gmail.com", 203512);
        Ventas ventas = new Ventas("autonomo", 2, 50, miFecha, 2500, "Prueba", "Prueba", cliente);

        Integer id = (Integer) session.save(ventas);
        session.getTransaction().commit();
        Assertions.assertFalse(id > 0);
    }

    @org.junit.jupiter.api.Test
    void getVenta() {

        System.out.println("Running testGet....");
        Integer id = 4;
        Date miFecha = new Date(115, 6, 2);
        Cliente cliente = new Cliente(7, "Perico", "Delgado", "75126694F", 205456851, miFecha, "Coso 201 25", "PericoDel@gmail.com", 203512);

        Ventas ventas = session.find(Ventas.class, id);
        Assertions.assertEquals(cliente, ventas.getCliente());
    }

    @org.junit.jupiter.api.Test
    void testUpdateVenta() {
        Integer id = 9;
        Date miFecha = new Date(115, 6, 2);
        Cliente cliente = new Cliente(7, "Perico", "Delgado", "75126694F", 205456851, miFecha, "Coso 201 25", "PericoDel@gmail.com", 203512);
        Ventas ventas = new Ventas(id,"empresa", 2, 50, miFecha, 2500, "Venta al gran ciclista Perico Delgado", "Prueba", cliente);

        session.beginTransaction();
        session.saveOrUpdate(ventas);
        session.getTransaction().commit();

        Ventas ventasUpd = session.find(Ventas.class, id);

        Assertions.assertEquals(2, ventasUpd.getCodVenta());
    }

    @org.junit.jupiter.api.Test
    void testDeleteVentas() {
        System.out.println("Running testDelete...");
        Integer id = 4;
        Ventas ventas = session.find(Ventas.class, id);
        session.beginTransaction();
        session.delete(ventas);
        session.getTransaction().commit();

        Ventas ventasDel = session.find(Ventas.class, id);

        Assertions.assertNull(ventasDel);
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}