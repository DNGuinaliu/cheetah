import com.davidng.cheetah.gui.Controlador;
import com.davidng.cheetah.gui.Modelo;
import com.davidng.cheetah.gui.Ventanas.Carga.VentanaCarga;
import org.hibernate.HibernateException;
import org.hibernate.Metamodel;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.metamodel.EntityType;

import java.util.Map;

public class Main {
    public static void main(String[] args) {
        VentanaCarga ventanaCarga = new VentanaCarga();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(modelo, ventanaCarga);
    }
}